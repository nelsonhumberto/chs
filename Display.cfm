<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>CHS Rounding</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="js/vendor2/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    <!-- font-awesome-4.7.0 -->
    <!-- http://fontawesome.io/examples/ -->
    <link href="font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet"> 
    <!---<script src="js/vendor/ie-emulation-modes-warning.js"></script>--->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->    
</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
    	<div class="container-fluid">
        	<div class="navbar-header">            
          		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#SideBar" aria-expanded="false" aria-controls="navbar">
            		<span class="sr-only">Toggle navigation</span>
            		<span class="icon-bar"></span>
            		<span class="icon-bar"></span>
            		<span class="icon-bar"></span>
          		</button>

        	</div>
            <!---
            <div id="navbar" class="nav navbar-nav navbar-right" style="padding-right:5px">
            	<a href="#" class="fa-align-right"><i class="fa fa-home"></i></a>
                <a href="#" class="fa-align-right"><i class="fa fa-info-circle"></i></a>
            </div>
			--->
        	<div id="navbar" class="navbar-collapse collapse">
          		<ul class="nav navbar-nav navbar-right">
                <!---
            		<li><a href="#"><i class="fa fa-home"></i></a></li>
            		<li><a href="#">Help</a></li>
				--->
          		</ul>
				
                <!---
          		<form class="navbar-form navbar-right">
            		<input type="text" class="form-control" placeholder="Search...">
          		</form>
				--->               
        	</div>
            
      	</div>
    </nav>

    <div class="container-fluid">
    	<div class="row">
        	<div id="SideBar" class="col-sm-3 col-md-2 sidebar">
            	
         		<ul class="nav nav-sidebar">
            		<li class="active"><a href="#">Menu <span class="sr-only">(current)</span></a></li>
                    <!---<li><a href="#"><i class="fa fa-home"></i> HOME</a></li>--->
                    <li><a href="#" id="idDashboardLink">Census</a></li>
                    <li><a href="#" id="idReportsLink">Reports</a></li>
          		</ul>
         
                <div id="idSearchFormDiv"></div>
        	</div>
        	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          		<!---<h1 class="page-header text-primary">Dashboard</h1>--->
                <div id="MainContentDiv"></div>
                <div id="ReportsDiv"></div>
        	</div>
      	</div>
        <nav class="nav navbar-default navbar-fixed-bottom" id="idNavFooter">
        </nav>
    </div>

    <!--- Bootstrap core JavaScript
    ================================================== --->
    <!--- Placed at the end of the document so the pages load faster --->
    <!---<script src="js/vendor/jquery.min.js"></script>--->
    <script src="js/vendor2/jquery-3.2.1.min.js"></script>
    <script src="js/vendor2/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <!---<script>window.jQuery || document.write('<script src="js/vendor/jquery.min.js"><\/script>')</script>--->
    <script src="js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/vendor/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/vendor/dataTables.bootstrap.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <!---<script src="js/vendor/holder.min.js"></script>--->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!---<script src="js/vendor/ie10-viewport-bug-workaround.js"></script>--->
    <script src="js/actions.js"></script>
  </body>
</html>
