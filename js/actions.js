/* Author's javascript code */

var TheShiftID;

/* ************************************************************************************************************* */
$("input:checkbox").click(function() {
    var bol = $("input:checkbox:checked").length >= 2;
    $("input:checkbox").not(":checked").attr("disabled",bol);
});

$( document ).ready( function() {

        $('[data-toggle="popover"]').popover();

   // if ( $( 'body' ).is( '.ICarePage' ) || $( 'body' ).is( '.LoginPage' ) ) {    

	//$('#MainContentDiv').load('Forms/CurrentCensus.cfm');
	$('#MainContentDiv').load('Forms/CurrentCensus.cfm', function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success")
			jsCurrentCensus();
			//CallDatePicker('idFromDateInput');
			$('.DatePicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
			//CheckUnit();
        if(statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
    	});	
	//$('#idNavFooter').load('DefaultFooter.cfm');
	$('#idSearchFormDiv').load('Forms/SearchForm.cfm');

	// clicking on the census link
	$('#idDashboardLink').click(function()
	{
		$('#idCurrentCensusDiv').hide();
		//$('#MainContentDiv').load('Forms/CurrentCensus.cfm',jsCurrentCensus);
		
		
		$('#MainContentDiv').load('Forms/CurrentCensus.cfm', function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success")
			jsCurrentCensus();
			//CallDatePicker('idFromDateInput');
			$('.DatePicker').datepicker();
			//CheckUnit();
        if(statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
    	});			
		
		$('#ReportsDiv').hide();
		$('#MainContentDiv').show();
		//$('#idNavFooter').load('DefaultFooter.cfm');
	});
		
	$('#idReportsLink').click(function()
	{
		//$('#MainContentDiv').load('<img src="img/img-loading.gif" id="ImgLoading" alt="loading" height="30" width="30">');
		$('#MainContentDiv').hide();
		$('#ReportsDiv').show();
		//$('#ReportsDiv').load('Forms/Reports.cfm',jsReportsCensus);
		
		$('#ReportsDiv').load('Forms/Reports.cfm', function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success")
			jsReportsCensus();
			$('.DatePicker').datepicker();
			//CheckUnit();
        if(statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
    	});	
		
		
		//$('.DatePicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
		//$('#ImgLoading').show(); 
		//$('#MainContentDiv').load('CurrentCensus.cfm');		
	})

	//$( function() {
    	//$( ".DatePicker" ).datepicker();
  	//} );
	

	/*
	$(function() {
    	$('.datepick').datepicker();
	});
	*/

} );


// Other options for date pickers
/*
$(document).on('click','.datepick',function(){
	//alert('test');
	$( ".datepick" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
	
});

function CallDatePicker(InputID)
{
	//alert(InputID);
	//$('#'+InputID).datepicker();	
	//setTimeout( "$('#idFromDateInput').datepicker();", 1000)
	$('#'+InputID).datepicker({ dateFormat: 'mm/dd/yy' }).val();	
	
	//setTimeout( "ShowAdminList('AttendingOnCall');", 1000)
				
				//var DTLength = GetTableLength('AttendingOnCall');			
				//setTimeout( "ShowAdminList('AttendingOnCall',"+DTLength+");", 1000)
}
*/

function LoadContent(content)
{
	$('#MainContentDiv').load(content+'.cfm');
}


function showLoading(DivID)
{
	$('#'+DivID).html('  <img src="img/img-loading.gif" width="20px" height="20px">  <a class="text-success">Processing...</a>');
	//$("#idAdminDiv").html('  <img src="img/img-loading.gif" width="20px" height="20px">  <a class="text-success">Processing...</a>');
}

function RunCurrentCensus()
{
	var FAC_IDInput = $('#idFAC_IDInput');
	var CellInput = $('#idCellInput');
	var PStatusInput = $('#idPStatus');
	var FromDateInput = $('#idFromDateInput');
	var ToDateInput = $('#idToDateInput');	
	
	//if (document.CensusReportsForm.PStatusInput.value == "")
	if (PStatusInput.val() == "")
	{
		alert('Please select inmate Status!');	
		//document.CensusReportsForm.PStatusInput.focus();
		PStatusInput.focus();
		return false;
	}	
	
	if (PStatusInput.val() == "R" & FromDateInput.val() == "") // document.CensusReportsForm.FromDate.value
	{
		alert('Please select a FROM date!');	
		//document.CensusReportsForm.FromDate.focus();
		FromDateInput.focus();
		return false;	
	}
	
	//if (document.CensusReportsForm.FromDate.value != "" & document.CensusReportsForm.ToDate.value == "")
	if (FromDateInput.val() != "" & ToDateInput.val() == "")
	{
		ToDateInput = FromDateInput;
	}
	
	var url = "Forms/CurrentCensus.cfm?RunReportButton=1&FAC_ID="+FAC_IDInput.val()+"&PStatus="+PStatusInput.val()+"&FromDate="+FromDateInput.val()+"&ToDate="+ToDateInput.val()+"&Cell_ID="+CellInput.val();
	
	showLoading('MainContentDiv');
	$('#MainContentDiv').load(url, function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success")
			jsCurrentCensus();
			CheckUnit();



        if(statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
    });	
}

function ClearRunCurrentCensus()
{
	$('#idFAC_IDInput').val('');
	$('#idCellInput').val('');
	$('#idPStatus').val('');
	$('#idFromDateInput').val('');
	$('#idToDateInput').val('');
}

function RunCensusReport()
{
	//alert('hello');
	var FAC_IDInput = $('#idReportFAC_IDInput');
	var CellInput = $('#idReportCellInput');
	var PStatusInput = $('#idReportPStatus');
	var FromDateInput = $('#idReportFromDateInput');
	var ToDateInput = $('#idReportToDateInput');
	var FromReportDate = $('#idFromReportDate');
	var ToReportDate = $('#idToReportDate');
	
	if (PStatusInput.val() == "")
	{
		alert('Please select inmate Status!');	
		//document.CensusReportsForm.PStatusInput.focus();
		PStatusInput.focus();
		return false;
	}	
	
	if (PStatusInput.val() == "R" & FromDateInput.val() == "") // document.CensusReportsForm.FromDate.value
	{
		alert('Please select a FROM date!');	
		//document.CensusReportsForm.FromDate.focus();
		FromDateInput.focus();
		return false;	
	}
	
	//if (document.CensusReportsForm.FromDate.value != "" & document.CensusReportsForm.ToDate.value == "")
	if (FromDateInput.val() != "" & ToDateInput.val() == "")
	{
		ToDateInput = FromDateInput;
	}
	
	var url = "Forms/Reports.cfm?RunReportButton=1&FAC_ID="+FAC_IDInput.val()+"&PStatus="+PStatusInput.val()+"&FromDate="+FromDateInput.val()+"&ToDate="+ToDateInput.val()+"&Cell_ID="+CellInput.val()+"&FromReportDate="+FromReportDate.val()+"&ToReportDate="+ToReportDate.val();
	//ColdFusion.navigate(url,'MainContentDiv',UpdatePStatus,'','POST','CensusReportsForm');
	
	
	showLoading('ReportsDiv');
	$('#ReportsDiv').load(url, function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success")
			jsReportsCensus();
			CheckReportUnit();
			$('.DatePicker').datepicker();
        if(statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
    });
	
}

// submit via "enter" key
function CheckEnterSubmit()
	{
		if (event.keyCode==13) 
		{
			//alert('Enter Key Pressed!');	
			LookupInmate();
			return false;
		}
	}
function LookupInmate()
{	
	//alert('hello');
	var JailNumber = $('#idSearchJailNumber');
	var FullName = $('#idSearchFullName');
	var CIN = $('#idSearchCIN');
	var DOB = $('#idSearchDOB');
	
	//alert(JailNumber);
	
	if (JailNumber.val() == "" & FullName.val() == "" & CIN.val() == "" & DOB.val() == "")
	{
		alert('Please enter a search value!');	
		JailNumber.focus();
		return false;	
	}
	
	var url = "Forms/LookupInmate.cfm?JailNumber="+JailNumber.val()+"&FullName="+FullName.val()+"&CIN="+CIN.val()+"&DOB="+DOB.val();
	//ColdFusion.navigate(url,'MainContentDiv',UpdatePStatus,'','POST','CensusReportsForm');
	
	$('#ReportsDiv').hide();
	$('#MainContentDiv').show();
	showLoading('MainContentDiv');
	$('#MainContentDiv').load(url, function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success")
			jsLookupInmate();
        if(statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
    });
	
}

function ClearLookupInmateForm()
{
	$('#idSearchJailNumber').val('');	
	$('#idSearchFullName').val('');
	$('#idSearchCIN').val('');
	$('#idSearchDOB').val('');
}
/*	
function LookupInmate2()
{
	var url = 'Forms/LookupInmate.cfm';
  	$.ajax
	({
    	url: url
    	,type: 'post'
    	,datatype: 'json'
    	,data:{ 
			'JailNumber': $('#idSearchJailNumber').val(),
			'FullName': $('#idSearchFullName').val(),
			'CIN': $('#idSearchCIN').val(),
			'DOB': $('#idSearchDOB').val()
      		,returnFormat: 'json'}
    	,success: function(data) 
		{
			var jsonParsed = $.parseJSON(data);
    		// success
			if ( jsonParsed.DATA.length > 0) 
			{				
				//Success		
				//$('#BH_MessageDiv').show();
				//$('#BH_MessageDiv').html('Changes saved successfully!')
				//$('#BH_MessageDiv').addClass('SuccessMessage');		 
			} 
			else 
			{
				// fail
				//$('#BH_MessageDiv').show();
				//$('#BH_MessageDiv').html('Fail!')
				//$('#BH_MessageDiv').addClass('FailMessage');
			 }
    	}
  	});
	
}
*/


function PickShiftBefore(ShiftID)
{
    $('#idSfift1').removeClass('TdProfileBlue');
    $('#idSfift2').removeClass('TdProfileBlue');
    $('#idSfift3').removeClass('TdProfileBlue');
    $('#idSfift1').addClass('TdProfileGrey');
    $('#idSfift2').addClass('TdProfileGrey');
    $('#idSfift3').addClass('TdProfileGrey');

    $('.cInputSfift1').prop('disabled', true);
    $('.cInputSfift2').prop('disabled', true);
    $('.cInputSfift3').prop('disabled', true);


    //if (ShiftID == 1)
    //{
    $('#idSfift'+ShiftID).removeClass('TdProfileGrey');
    $('#idSfift'+ShiftID).addClass('TdProfileBlue');
    $('.cInputSfift'+ShiftID).prop('disabled', false);

    //}

    // set value
    $('#idShiftID').val(ShiftID);
    $('#idSaveButton_BH_Sheet').prop('disabled', false);
    $('#idSaveSearchButton_BH_Sheet').prop('disabled', false);
    TheShiftID = ShiftID;

}








function SelectInmateProfile(JailNumber){


	//$('#ModalHeader_BH_Sheet').html('<div class="well" align="center" style="font-size:16; font-weight:bold; background-color:#5eace8">EDIT '+JailNumber+'</div>')
	var tab = 'idCurrentDiv';




    $('.nav-tabs a[href="#' + tab + '"]').tab('show');

    //$('#idCurrentDiv').focus();
	//$('#idHistoryDiv').hide();
	$('#idCurrentDiv').load('Forms/InmateProfile.cfm?JailNumber='+JailNumber);

	$('#idSaveButton_BH_Sheet').show();
	$('#idCancelButton_BH_Sheet').show(); 
	$('#idCurrentJailNumber').val(JailNumber);
	//$('#idHistoryDiv').load('Forms/InmateRoundingHistory.cfm?JailNumber='+JailNumber);







}
// use this to get rid of extra coding: if(JailNumber == undefined){var JailNumber = $('#idCurrentSearchJailNumber').val();};
/*
function ShowInmateProfile(){
	var JailNumber = $('#idCurrentJailNumber').val();	
	//$('#ModalHeader_BH_Sheet').html('<div class="well" align="center" style="font-size:16; font-weight:bold; background-color:#5eace8">EDIT '+JailNumber+'</div>')
	$('#idCurrentDiv').load('Forms/InmateProfile.cfm?JailNumber='+JailNumber);
	//$('#idCurrentDiv').show();
	//$('#idHistoryDiv').hide();
	$('#idSaveButton_BH_Sheet').show();
	$('#idCancelButton_BH_Sheet').show(); 
	$('#idCurrentJailNumber').val(JailNumber);
	//$('#idHistoryDiv').load('Forms/InmateRoundingHistory.cfm?JailNumber='+JailNumber);
}
*/
function ShowInmateBHHistory(){
	var JailNumber = $('#idCurrentJailNumber').val();
	$('#idHistoryDiv').load('Forms/InmateRoundingHistory.cfm?JailNumber='+JailNumber, jsRoundingHistory);
	//$('#idHistoryDiv').show();
	//$('#idCurrentDiv').hide();
	$('#idSaveButton_BH_Sheet').hide();
	$('#idCancelButton_BH_Sheet').hide(); 
}

function ShowReportInmateProfile(JailNumber){

if (JailNumber == undefined) {
        var JailNumber = $('#idCurrentReportJailNumber').val();
    }
    var tab = 'idCurrentReportDiv';
	$('.nav-tabs a[href="#' + tab + '"]').tab('show');	
	//$('#ReportModalHeader_BH_Sheet').html('<div class="well" align="center" style="font-size:16; font-weight:bold; background-color:#5eace8">EDIT '+JailNumber+'</div>')
	$('#idCurrentReportDiv').load('Forms/InmateProfile.cfm?JailNumber='+JailNumber);
	$('#idCurrentReportJailNumber').val(JailNumber);
	//$('#idSaveButton_BH_Sheet').hide();
	//$("input[name='Shift']").attr("disabled", true);
	//$("input[name='Shift']").prop( "disabled", true );
	$('input[name="Shift"]').attr('disabled', 'disabled');
}

function ShowReportInmateBHHistory(){
	var JailNumber = $('#idCurrentReportJailNumber').val();
	$('#idHistoryReportDiv').load('Forms/InmateRoundingHistory.cfm?JailNumber='+JailNumber, jsRoundingHistory);
	//$('#idHistoryDiv').show();
	//$('#idCurrentDiv').hide();
	$('#idSaveReportButton_BH_Sheet').hide();
	//$('#idCanceSearchlButton_BH_Sheet').hide(); 
}

function ShowSearchInmateProfile(JailNumber){
    if (JailNumber == undefined) {
        var JailNumber = $('#idCurrentSearchJailNumber').val();
    }
    var tab = 'idCurrentSearchDiv';
	$('.nav-tabs a[href="#' + tab + '"]').tab('show');	
	//$('#SearchModalHeader_BH_Sheet').html('<div class="well" align="center" style="font-size:16; font-weight:bold; background-color:#5eace8">EDIT '+JailNumber+'</div>')
	$('#idCurrentSearchDiv').load('Forms/InmateProfile.cfm?JailNumber='+JailNumber);
	$('#idCurrentSearchJailNumber').val(JailNumber);
	$('#idSaveSearchButton_BH_Sheet').show();
	$('#idCanceSearchlButton_BH_Sheet').show();
}

function ShowSearchInmateBHHistory(){
	var JailNumber = $('#idCurrentSearchJailNumber').val();
	$('#idHistorySearchDiv').load('Forms/InmateRoundingHistory.cfm?JailNumber='+JailNumber, jsRoundingHistory);
	//$('#idHistoryDiv').show();
	//$('#idCurrentDiv').hide();
	$('#idSaveSearchButton_BH_Sheet').hide();
	$('#idCanceSearchlButton_BH_Sheet').hide(); 
}

function jsCurrentCensus() {
  $('#idCurrentCensus').dataTable({
    "iDisplayLength": 20,
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "oLanguage": { "sLengthMenu":
      '<select>'+
      '<option value="5">5</option>'+
      '<option value="10">10</option>'+
      '<option value="20">20</option>'+
      '<option value="30">30</option>'+
      '<option value="40">40</option>'+
      '<option value="50">50</option>'+
      '<option value="-1">All</option>'+
      '</select>',
      "bStateSave": true
      }
  });
}

function jsReportsCensus() {
  $('#idReportsCensus').dataTable({
    "iDisplayLength": 20,
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "oLanguage": { "sLengthMenu":
      '<select>'+
      '<option value="5">5</option>'+
      '<option value="10">10</option>'+
      '<option value="20">20</option>'+
      '<option value="30">30</option>'+
      '<option value="40">40</option>'+
      '<option value="50">50</option>'+
      '<option value="-1">All</option>'+
      '</select>',
      "bStateSave": true
      }
  });
}

function jsLookupInmate() {
  $('#idLookupInmateDT').dataTable({
    "iDisplayLength": 20,
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "oLanguage": { "sLengthMenu":
      '<select>'+
      '<option value="5">5</option>'+
      '<option value="10">10</option>'+
      '<option value="20">20</option>'+
      '<option value="30">30</option>'+
      '<option value="40">40</option>'+
      '<option value="50">50</option>'+
      '<option value="-1">All</option>'+
      '</select>',
      "bStateSave": true
      }
  });
}


function jsRoundingHistory() {
  $('#idRoundingHistoryDT').dataTable({
    "iDisplayLength": 20,
	"order":[[0, "desc"]],
    "bDeferRender": true,
    "sPaginationType": "full_numbers",
    "oLanguage": { "sLengthMenu":
      '<select>'+
      '<option value="5">5</option>'+
      '<option value="10">10</option>'+
      '<option value="20">20</option>'+
      '<option value="30">30</option>'+
      '<option value="40">40</option>'+
      '<option value="50">50</option>'+
      '<option value="-1">All</option>'+
      '</select>',
      "bStateSave": true
      }
  });
}


function CheckUnit()
{
	var FACID = $('#idFAC_IDInput').val();
				
	if(FACID == 'K')
	{
		$('#idCellInput').show()	
	}
	else
	{
		$('#idCellInput').hide()	
	}
}

function CheckReportUnit()
{
	var FACID = $('#idReportFAC_IDInput').val();
				
	if(FACID == 'K')
	{
		$('#idReportCellInput').show()	
	}
	else
	{
		$('#idReportCellInput').hide()	
	}
}



// test





function PickShift(ShiftID)
{
	//alert(ShiftID);
	//Reset
	$('#idSfift1').removeClass('TdProfileBlue');
	$('#idSfift2').removeClass('TdProfileBlue');
	$('#idSfift3').removeClass('TdProfileBlue');
	$('#idSfift1').addClass('TdProfileGrey');
	$('#idSfift2').addClass('TdProfileGrey');
	$('#idSfift3').addClass('TdProfileGrey');
    $('#idSfiftTable'+ShiftID).removeClass('TdProfileBlue');
    $('#idSfiftTable'+ShiftID).addClass('TdProfileGrey');


    $('#idSfift1').css('display','none');
    $('#idSfift2').css('display','none');
    $('#idSfift3').css('display','none');
    $('#idSfiftTable'+ShiftID).css('display','none');




	$('.cInputSfift1').prop('disabled', true);
	$('.cInputSfift2').prop('disabled', true);
	$('.cInputSfift3').prop('disabled', true);
	
	//if (ShiftID == 1)
	//{
		$('#idSfift'+ShiftID).removeClass('TdProfileGrey');
		$('#idSfift'+ShiftID).addClass('TdProfileBlue');
        $('#idSfift'+ShiftID).css('display','block');
        $('#idSfiftTable'+ShiftID).css('display','table-row');

    $('.cInputSfift'+ShiftID).prop('disabled', false);
	//}
	
	// set value
	$('#idShiftID').val(ShiftID);
	$('#idSaveButton_BH_Sheet').prop('disabled', false);
	$('#idSaveSearchButton_BH_Sheet').prop('disabled', false);
}


function Save_BH_Rounding()
{
	var ShiftID = $('#idShiftID').val();
	//alert($('#idRecordID'+ShiftID).val());

	var url = 'cfc/ManageBHRounding.cfc?method=AddUpdateBHRounding';
  	$.ajax
	({
    	url: url
    	,type: 'post'
    	,datatype: 'json'
    	,data:{ 
      		'RecordID': $('#idRecordID'+ShiftID).val(),
			'ShiftID':ShiftID,
			'JailNumber': $('#idJailNumber').val(),
			'CELL': $('#idCELL').val(),
			'LVL': $('#idLVL').val(),
			'Diagnosis': $('#idDiagnosis').val(),
            'One_2_One': $('#idOne_2_One'+ShiftID).is(':checked'),
			'Assaultive': $('#idAssaultive'+ShiftID).is(':checked'),
			'Sharps': $('#idSharps'+ShiftID).is(':checked'),
			'Falls': $('#idFalls'+ShiftID).is(':checked'),
			'Seizures': $('#idSeizures'+ShiftID).is(':checked'),
			'ChangeInCondition': $('#idChangeInCondition'+ShiftID).is(':checked'),
			'NewOrders': $('#idNewOrders'+ShiftID).is(':checked'),
			'MedRefusal': $('#idMedRefusal'+ShiftID).is(':checked'),
			'Etos': $('#idEtos'+ShiftID).is(':checked'),
			'Prns': $('#idPrns'+ShiftID).is(':checked'),
			'Detox': $('#idDetox'+ShiftID).is(':checked'),
			'Diabetes': $('#idDiabetes'+ShiftID).is(':checked'),
			'Pregnant': $('#idPregnant'+ShiftID).is(':checked'),			
			'WoundCare': $('#idWoundCare'+ShiftID).is(':checked'),
			'IVF': $('#idIVF'+ShiftID).is(':checked'),
			'IVABTS': $('#idIVABTS'+ShiftID).is(':checked'),
			'Iso': $('#idIso'+ShiftID).is(':checked'),
			'MealRecord': $('#idMealRecord'+ShiftID).is(':checked'),			
			'Comments': $('#idComments'+ShiftID).val(),
			'MedicalHistory': $('#idMedicalHistory'+ShiftID).val(),
			'Procedures': $('#idProcedures'+ShiftID).val(),
			'ChangeInConditionText': $('#idChangeInConditionText'+ShiftID).val(),
			'NewOrdersText': $('#idNewOrdersText'+ShiftID).val(),
			'MedRefusalText': $('#idMedRefusalText'+ShiftID).val(),
			'EtosText': $('#idEtosText'+ShiftID).val(),
			'PrnsText': $('#idPrnsText'+ShiftID).val(),
			'DetoxText': $('#idDetoxText'+ShiftID).val(),
			'DiabetesText': $('#idDiabetesText'+ShiftID).val(),
			'PregnantText': $('#idPregnantText'+ShiftID).val(),
			'WoundCareText': $('#idWoundCareText'+ShiftID).val(),
			'IVFText': $('#idIVFText'+ShiftID).val(),
			'IVABTSText': $('#idIVABTSText'+ShiftID).val(),
			'IsoText': $('#idIsoText'+ShiftID).val(),
			'MealRecordText': $('#idMealRecordText'+ShiftID).val(),
			'Labs': $('#idLabs'+ShiftID).is(':checked'),
			'LabsText': $('#idLabsText'+ShiftID).val(),
			'Ciwa': $('#idCiwa'+ShiftID).is(':checked'),
			'CiwaText': $('#idCiwaText'+ShiftID).val(),
			'OutOfCell': $('#idOutOfCell'+ShiftID).is(':checked'),
			'OutOfCellText': $('#idOutOfCellText'+ShiftID).val(),
            'Suicide': $('#idSuicide'+ShiftID).is(':checked'),
            'SuicideText': $('#idSuicideText'+ShiftID).val(),
            'LevelText': $('#idLevelText'+ShiftID).val(),
            'Admitting': $('#idAdmitting'+ShiftID).val(),
            'Diagnostic': $('#idDiagnostic'+ShiftID).val()



            ,returnFormat: 'json'}
    	,success: function(data) 
		{
			var jsonParsed = $.parseJSON(data);
    		// success
			if ( jsonParsed.DATA.length > 0) 
			{				
				var JailNumber = jsonParsed.DATA[0][1];			
				//alert(JailNumber);				
				//SelectInmateProfile($('#idJailNumber').val())
				SelectInmateProfile(JailNumber);
				
				setTimeout( "$('#BH_MessageDiv').show(); $('#BH_MessageDiv').html('<strong>Changes saved successfully!</strong>'); $('#BH_MessageDiv').addClass('SuccessMessage');", 1000);
				
				// diable button to rest form
				$('#idSaveButton_BH_Sheet').prop('disabled', true);
				//$('#BH_MessageDiv').show();
				//$('#BH_MessageDiv').html('<strong>Changes saved successfully!</strong>')
				//$('#BH_MessageDiv').addClass('SuccessMessage');
				//setTimeout( "ShowAdminList('AttendingOnCall');", 1000)
				 
			} 
			else 
			{
				// fail
				$('#BH_MessageDiv').show();
				$('#BH_MessageDiv').html('Fail!');
				$('#BH_MessageDiv').addClass('FailMessage');
			 }
    	}
  	});
	
}


/*	---------------------------------------------------------------------------------------------------------------------------------------	*/
/*

function SelectLocation()
{
 //var url = "SelectLocation.cfm"	
 $('#idCurrentCensusDiv').load('SelectLocation.cfm');
 
}


$(document).on('click','#idTestMe',function(){
	alert('test');
	
	
	
	var oTable = $('#idCurrentCensus3').dataTable( {
    //The CFC source of the data
    "ajax": 'cfc/ManageIround.cfc?method=GetCurrentPtCensus2', 
    "columns": [                                            
        {"data": "PTNAME", "width": "50%"},
        {"data": "ROOMBED", "width": "50%"}
    ]
	
	});
	
	
});


// Manny's start
//Called from SelectLocation
function getUnitCensus( pFacility ) {
  // if Census Facility <> Other make the ajax call
  if (  pFacility !== '- Other -' ) { 
    // show image 'loading'
    $('#UnitImgLoading').show();    
    var url = 'cfc/ManageIround.cfc?method=getUnitCensus';
    $.ajax( {
      url: url
      ,type: 'post'
      ,datatype: 'json'
      ,data: { 'Facility': pFacility, returnFormat: 'json' }
      ,success: function( data ) {
        // data = data.replace( '"COLUMNS":["UNIT"],"DATA":', '' );
        var jsonParsed = $.parseJSON( data );
        // console.log( typeof( jsonParsed ) ); console.log( jsonParsed ); console.log( jsonParsed.DATA.length );
        $('#idUnit').find( 'option' ).remove().end().append( '<option value="">- select one unit -</option>' ); 
        for ( var x = 0; x < jsonParsed.DATA.length; x++ ) {
          // console.log( jsonParsed.DATA[x][0] );
          $('#idUnit').append( $( '<option>', {
            value: jsonParsed.DATA[x][0],
            text: jsonParsed.DATA[x][0]
          } ) );
        }
      },
      // hide image 'loading' and enable select 
      complete: function(){
        $('#UnitImgLoading').hide();
        $('#idUnit').prop( 'disabled', false );        
      }
    } );
  // if Census Facility = Other then Census Unit = Other and Select a patient (census) = Other  
  } else { 
    $('#idUnit').find( 'option' ).remove().end().append( '<option value="- Other -">- Other -</option>' ); 
    $('#idUnit').prop( 'disabled', true );
    $('#idPatientCensus').find( 'option' ).remove().end().append( '<option value="- Other -">- Other -</option>' ); 
    $('#idPatientCensus').prop( 'disabled', true );    
  }
}

function getPatientCensus( pId ) {
  // lodaing image
  $( '#PtImgLoading' + pId ).show();    
  var url = 'cfc/npsg-main.cfc?method=getPatientCensus';
  $.ajax( {
    url: url
    ,type: 'post'
    ,datatype: 'json'
    ,data: { 
      'Facility': $( '#idFacility' + pId ).val()
      ,'Unit': $( '#idUnit' + pId ).val()
      ,returnFormat: 'json' }
    ,success: function( data ) {
      var jsonParsed = $.parseJSON( data );
      $( '#idPatientCensus' + pId ).find( 'option' ).remove().end().append( '<option value="">- select one patient -</option>' ); 
      for ( var x = 0; x < jsonParsed.DATA.length; x++ ) {
        $( '#idPatientCensus' + pId ).append( $( '<option>', {
          value: jsonParsed.DATA[x][0],
          text: jsonParsed.DATA[x][0]
        } ) );
      }
    },
    // hide image 'loading' and enable select
    complete: function(){
      $( '#PtImgLoading' + pId ).hide();
      $( '#idPatientCensus' + pId ).prop( 'disabled', false );        
    }    
  } );
}
// Manny's end

function GetCenus()
{
	var Facility = $( '#idFacility' ).val();
	var Unit = $( '#idUnit' ).val();
	
	if (Facility == "" || Unit == "")
	{
		alert('Missing Facility & Unit');	
		return false;
	}
	
	//alert('Facility= '+Facility + ' & Unit = ' + Unit);
	//$('#idCurrentCensusDiv').unload('SelectLocation.cfm');
	//idRunButton
	
	GetCurrentCensus(Facility,Unit);
	
}

function GetCurrentCensus(Facility,Unit){
	var url = 'cfc/ManageIround.cfc?method=GetCurrentPtCensus2';
	//jQuery - AJAX get()
	//$('#ImgLoading').show();
	//$('#MainContent').html('<img src="img/img-loading.gif" id="ImgLoading" alt="loading" height="30" width="30">');
	
	//alert(Facility + ' -- ' + Unit)
	
	
	if ( $.fn.dataTable.isDataTable( '#idCurrentCensus' ) ) 
	{
    	table = $('#idCurrentCensus').DataTable();
	}
	else 
	{
		table = $('#idCurrentCensus').DataTable( {
			paging: false
		} );
		
		var aSelected = [];
		
		var table = $('<table id="idCurrentCensus" class="table table-striped table-hover" width="100%"><thead><tr><th>NAME</th><th>ROO/MBED</th><th>FIN</th><th></th></tr></thead><tbody></tbody></table>').appendTo("#idCurrentCensusDiv");
			//$('#ImgLoading').hide();			
			//jsCurrentCensus();			
			$('#idCurrentCensus').dataTable( 
			{				
				"iDisplayLength": 10,		
				"bDeferRender": true, 					
				//"sPaginationType": "full_numbers",		
				"ajax": 'cfc/ManageIround.cfc?method=GetCurrentPtCensus2&Facility='+Facility+'&Unit='+Unit, 
    			"columns": [                                            
        		{"data": "PTNAME", "width": "30%"},
        		{"data": "ROOMBED", "width": "30%"},
				{"data": "FIN", "width": "30%"},				
				{"data": "", "width": "10%", 
					"render": function(data,type,row,meta){
						var a = '<i class="fa fa-bookmark text-success"></i> <i class="fa fa-hourglass text-primary"> <a href="#" onclick="alert('+row.FIN+')"><i class="fa fa-edit"></i></a>';
						return a;
					}					
				},
   				],				
						
			});			
	}
}





// Call Census Report
function CallBackJQ_ReportCensus_DTID() 
{
	//alert('test');
	jQuery('#ReportCensus_DTID').dataTable({
		//"sDom": "<'row'<'span8'l><'span8'f>r>t<'row'<'span8'i><'span8'p>>",
		"sPaginationType": "full_numbers"
		//,"oLanguage": { "sLengthMenu": "" }
		//,"bLengthChange": false
		,"iDisplayLength": 5
		//,"sLengthMenu": [ [5, 10, 20, -1], [5, 10, 20, "All"] ]
		,"oLanguage": { "sLengthMenu": "Display _MENU_ records" }
		,"oLanguage": {
			"sLengthMenu": '<select>'+        
			'<option value="5">5</option>'+        
			'<option value="10">10</option>'+        
			'<option value="20">20</option>'+               
			'<option value="-1">All</option>'+        
			'</select>'
			}

	});
	
}
	
*/
	
/* ************************************************************************************************************* */





/*
var callNpsgPage = function() {
  var mainUrl = 'https://jhsmiami.org/iRound/Display.cfm';
  window.location = mainUrl; 
}

var callLoginPage = function( pUserName ){
  var loginUrl = 'https://jhsmiami.org/iRound/login.cfm?Logoff=1';
  window.location = loginUrl; 
}

var callICarePage = function(){
  var mainUrl = '../iRound/Display.cfm';
  window.location = mainUrl; 
}
*/

var valByIdText = function() {
  var textEmpty = 0;
  if ( arguments.length > 0 ) {  
    for ( var i = 0; i < arguments.length; i++ ) {
      if ( $.trim( $( '#' + arguments[i]).val() ).length === 0 ) {      
        $( '#' + arguments[i]).closest( 'div' ).addClass( 'has-error has-feedback' ).removeClass( 'has-success' );
        $( '#' + arguments[i]).next( 'span' ).removeClass( 'hidden fa-check' ).addClass( 'fa-exclamation-triangle' );
        textEmpty ++;
      } else {
        $( '#' + arguments[i] ).closest( 'div' ).addClass( 'has-success has-feedback' ).removeClass( 'has-error' );
        $( '#' + arguments[i] ).next( 'span' ).removeClass( 'hidden fa-exclamation-triangle' ).addClass( 'fa-check' );
      }
    }
    if ( textEmpty > 0 ) {
      return textEmpty
    } 
    else {
      return 0
    }
  }  
};


function getValueUsingClass(){
    /* declare an checkbox array */
    var chkArray = [];

    /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
    $(".chk:checked").each(function() {
        chkArray.push($(this).val());
    });

    /* we join the array separated by the comma */
     var selected;
     selected = chkArray.join(',') + ",";

    /* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
    if(selected.length > 1){
        SelectInmateProfileGroup(chkArray);
    }else{
        alert("Please at least one of the checkbox");
    }
}



function SelectInmateProfileGroup(JailNumber){
    //$('#ModalHeader_BH_Sheet').html('<div class="well" align="center" style="font-size:16; font-weight:bold; background-color:#5eace8">EDIT '+JailNumber+'</div>')
    var tab = 'idCurrentDiv';

    $('.nav-tabs a[href="#' + tab + '"]').tab('show');

    //$('#idCurrentDiv').focus();
    //$('#idHistoryDiv').hide();
    $('#idCurrentDiv').load('Forms/GroupProfile.cfm?JailNumber='+JailNumber);
    $('#idSaveButton_BH_Sheet').show();
    $('#idCancelButton_BH_Sheet').show();
    $('#idCurrentJailNumber').val(JailNumber);
    //$('#idHistoryDiv').load('Forms/InmateRoundingHistory.cfm?JailNumber='+JailNumber);
}
