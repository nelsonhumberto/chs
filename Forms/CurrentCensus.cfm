<!---<cfdump var="#FORM#">--->






<cfparam name="FAC_ID" default="">
<cfparam name="Cell_ID" default="">
<cfparam name="PStatus" default="">
<cfparam name="FromDate" default="">
<cfparam name="ToDate" default="">

<!--- extended the timeout to 5 minutes --->
<cfsetting requesttimeout="300">

<cfquery name="spListFacilities_Return" datasource="#REQUEST.Datasource#">
	exec CHS.dbo.spListFacilities       
</cfquery> 

<cfif NOT isDefined('RunReportButton')>
	<cfset FAC_ID = 0>
</cfif>

<cfquery name="spRunCensusReport_Return" datasource="#REQUEST.Datasource#">
	exec CHS_Dev.dbo.spRunCurrentCensus_BH_Rounding
    	@FAC_ID = <cfqueryparam value="#TRIM(FAC_ID)#" cfsqltype="cf_sql_varchar">,
        @Cell_ID = <cfqueryparam value="#TRIM(Cell_ID)#" cfsqltype="cf_sql_varchar">,
        @PStatus = <cfqueryparam value="#TRIM(PStatus)#" cfsqltype="cf_sql_varchar">,
        @FromDate = <cfqueryparam value="#TRIM(DateFormat(FromDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">,
        @ToDate = <cfqueryparam value="#TRIM(DateFormat(ToDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">
</cfquery>

<div class="row placeholders">
    <div class="embed-responsive-item-16by9">
    </div>
</div>

<h3 class="sub-header text-primary">Current Census</h3>

<div class="table-responsive">
    <table class="table table-striped table-hover"><!---	f9f9f9	--->
    	<tr bgcolor="#f9f9f9" valign="top">
        	<cfform name="CensusReportsForm" method="post">
        	<td valign="top">            
            	<strong>Facility</strong>
            	<cfselect name="FAC_ID" id="idFAC_IDInput" query="spListFacilities_Return" queryPosition="below" value="FAC_ID" display="FAC_Name" selected="#FAC_ID#" onchange="CheckUnit()">
                	<option value="">All Facilities</option>
                </cfselect>
                <!---<div class="form-group" style="margin-right:200px; float:right">--->
                	<!---<label for="idCellInput">Select Unit:</label>--->
                  	<select class="form-control" name="Cell" id="idCellInput" style="display:none; width:160px; float:right" multiple="multiple">
                        <option value="">Pick Unit</option>
                        <option value="k10" <cfif ListFind(Cell_ID,'k10')>selected="yes"</cfif>>k10</option>
                        <option value="K21" <cfif ListFind(Cell_ID,'K21')>selected="yes"</cfif>>K21</option>                    
                        <option value="K22" <cfif ListFind(Cell_ID,'K22')>selected="yes"</cfif>>K22</option>                    
                        <option value="K23" <cfif ListFind(Cell_ID,'K23')>selected="yes"</cfif>>K23</option>
                        <option value="K24" <cfif ListFind(Cell_ID,'K24')>selected="yes"</cfif>>K24</option>
                        <option value="K31" <cfif ListFind(Cell_ID,'K31')>selected="yes"</cfif>>K31</option>
                        <option value="K32" <cfif ListFind(Cell_ID,'K32')>selected="yes"</cfif>>K32</option>
                        <option value="K33" <cfif ListFind(Cell_ID,'K33')>selected="yes"</cfif>>K33</option>
                        <option value="K34" <cfif ListFind(Cell_ID,'K34')>selected="yes"</cfif>>K34</option>
                        <option value="K41" <cfif ListFind(Cell_ID,'K41')>selected="yes"</cfif>>K41</option>
                        <option value="K43" <cfif ListFind(Cell_ID,'K43')>selected="yes"</cfif>>K43</option>
                        <option value="K46" <cfif ListFind(Cell_ID,'K46')>selected="yes"</cfif>>K46</option>
                        <option value="K51" <cfif ListFind(Cell_ID,'K51')>selected="yes"</cfif>>K51</option>
                        <option value="K53" <cfif ListFind(Cell_ID,'K53')>selected="yes"</cfif>>K53</option>
                        <option value="k111" <cfif ListFind(Cell_ID,'K111')>selected="yes"</cfif>>k111</option>
                        <option value="k110" <cfif ListFind(Cell_ID,'K110')>selected="yes"</cfif>>k110</option>
                        <option value="Infirmary" <cfif ListFind(Cell_ID,'Infirmary')>selected="yes"</cfif>>Infirmary</option>
                        
                        
                	</select>
               <!--- </div>	--->
                
            </td>
            <td>
            	<strong>Status</strong><br>
            	<select name="PStatus" id="idPStatus">
                	<option value="">Pick One</option>
                    <option value="R" <cfif PStatus eq 'R'>selected="selected"</cfif>>Released</option>
                    <option value="N" <cfif PStatus eq 'N'>selected="selected"</cfif>>Incarcerated</option>
                </select>
            </td>
            <td><div style="position:relative; z-index:1" id="FromDateID">
            	<strong>From</strong><br>
            	<cfoutput><input type="date" class="DatePicker" name="FromDate" id="idFromDateInput" size="10" value="#DateFormat(FromDate,'yyyy-MM-dd')#" title="FROM DATE"></cfoutput></div>
            </td>
            <td><div style="position:relative; z-index:1" id="ToDateID">      
            	<strong>To</strong><br>
            	<cfoutput><input type="date" class="DatePicker" name="ToDate" id="idToDateInput" value="#DateFormat(ToDate,'mm/dd/yyyy')#" size="10" title="TO DATE"></cfoutput></div>
            </td>
        </tr>
        <tr bgcolor="#f9f9f9">
        	<td colspan="2">
            	<cfinput type="button" name="RunCensusReportButton" value=" Run " class="button blue radius small nice" onClick="RunCurrentCensus()"/>
            	<cfinput type="button" name="ResetCensusReportButton" value="Reset Values" class="button blue radius small nice" onClick="ClearRunCurrentCensus()"/>
            </td>
            <td></td>
            <td>
            <cfoutput>
			<cfif spRunCensusReport_Return.RecordCount GT 0>
                    <!---<a href="Forms/PrintCurrentCensus.cfm?FAC_ID=#FAC_ID#&amp;Cell_ID=#Cell_ID#&amp;PStatus=#PStatus#&amp;FromDate=#FromDate#&amp;ToDate=#ToDate#" target="_blank"><img src="img/export_excel.png" width="32" height="32" alt="Export to Excel" /></a>&nbsp;&nbsp;--->
                    <a href="Forms/PrintCurrentCensusPDF.cfm?FAC_ID=#FAC_ID#&amp;Cell_ID=#Cell_ID#&amp;PStatus=#PStatus#&amp;FromDate=#FromDate#&amp;ToDate=#ToDate#" target="_blank"><img src="img/pdf_icon.png" width="50" height="50" alt="Export to PDF" /></a>&nbsp;&nbsp;
                    <!---<a href="Forms/PrintCurrentMDLCensus.cfm?FAC_ID=#FAC_ID#&amp;Cell_ID=#Cell_ID#&amp;PStatus=#PStatus#&amp;FromDate=#FromDate#&amp;ToDate=#ToDate#" target="_blank"><img src="img/export_excel.png" width="32" height="32" alt="Export to Excel" />MDL</a>--->
            </cfif>
            </cfoutput>
            </td>
    	</tr>
        </cfform>
        <tr>
        	<td colspan="4">		
              	<a name="CallData" id="CallData">Showing <font color="##006699">&nbsp;</font> <cfoutput>#spRunCensusReport_Return.recordCount#</cfoutput><font color="##006699">&nbsp;</font> Records</a>
          </td>
        </tr>
    </table>
	<table id="idCurrentCensus" class="table table-striped table-hover">
    	<thead>

        <tr>

            <!---<th>Select</th>--->

            <th>Name</th>
                <th>DOB</th>
                <th>JailNumber</th>
                <th>Facility</th>
                <th>Cell</th>
                <th>Booked</th>
                <th>Released</th>
            </tr>

        </thead>
<!------>
<!------>
                                                                           <!---<div id="RadioShift" style="display: none">  Choose a Shift--->
                            <!---<label><input type="radio" name="Shift" class="RadioButton" onclick="PickShiftBefore('1')"  > 7X3</label>&nbsp;--->
                            <!---<label><input type="radio" name="Shift" class="RadioButton" onclick="PickShiftBefore('2')" > 3X11</label>&nbsp;--->
                            <!---<label><input type="radio" name="Shift" class="RadioButton" onclick="PickShiftBefore('3')" > 11X7</label>&nbsp;--->
<!------>
<!------>
                                                                                <!---</div>--->
<!------>
<!---<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Modal_BH_Sheet" onClick="getValueUsingClass();">Select 4 Reports Rounding</button>--->

    <br> <br>

<tbody>
        	<cfoutput query="spRunCensusReport_Return">
        	<tr>
                <!---<td ><input value="#JailNumber#" type="checkbox" class="chk"  onClick="return KeepCount(this)"> </td>--->
                <td><a href="##" data-toggle="modal" data-target="##Modal_BH_Sheet" onClick="javaScript:SelectInmateProfile('#JailNumber#');">#FullName#</a></td>                <!---<td align="center">#Gender#</td>--->
                <td align="center">#DateFormat(DOB,'mm/dd/yyyy')#</td>
               	<td align="center">#JailNumber#</td>
                <!---<td align="center">#FIN#</td>--->
                <td>#FAC_Name#</td>
                <td align="center">#Cell#</td>
                <td align="center">#DateFormat(Book_DT,"mm/dd/yyyy")#</td>
                <td align="center">#DateFormat(Rel_DT,"mm/dd/yyyy")#</td>
            </tr>
            </cfoutput> 
        </tbody>
	</table>
</div>



<!--- Modal --->
<!---<cfoutput></cfoutput>--->
<div id="Modal_BH_Sheet" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="display:none">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="ModalHeader_BH_Sheet"></h4>
        <!---<div class="well" align="center" style="font-size:16; font-weight:bold; background-color:#5eace8">CASE DETAIL</div>--->
      </div>
      <div class="modal-body" id="ModalBody_BH_Sheet">
      	 <ul class="nav nav-tabs">
         	<li class="active"><a data-toggle="tab" href="#idCurrentDiv" onclick="SelectInmateProfile();">Today</a></li>
            <li><a data-toggle="tab" href="#idHistoryDiv" onclick="ShowInmateBHHistory();">Rounding History</a></li>
         </ul>
         <div class="tab-content">
         	<div id="idCurrentDiv" class="tab-pane fade in active"></div>
            <div id="idHistoryDiv" class="tab-pane fade"></div>
         </div>
      </div>
      <!------>
	  <div class="modal-footer">
        <input type="button" class="btn btn-success" name="SaveButton_BH_Sheet" id="idSaveButton_BH_Sheet" onClick="Save_BH_Rounding();" value=" SAVE " style="float:left" disabled="disabled" />
        <input type="button" class="btn btn-danger" name="CancelButton_BH_Sheet" id="idCancelButton_BH_Sheet" data-dismiss="modal" value=" Cancel " style="float:left" />
        <input type="hidden" name="CurrentJailNumber" id="idCurrentJailNumber" value="" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!---
<div class="table-responsive">
    <table class="table table-striped table-hover">
      <thead>
        <tr>
            <th>Facility</th>                
            <th>Capacity</th>
            <th>Census</th>
            <th>%</th>
        </tr>
      </thead>
      <tbody>
       <cfoutput query="GetCensusReport">
        <tr class="#Class#">
            <td align="center">#Facility#</td>
            <td align="center">#FunctionalCapacity#</td>
            <td>#TotalCensus#</td>
            <td>#CurrentCensusCapacity#%</td>
        </tr>
        </cfoutput>                            
      </tbody>
    </table>
</div>
--->



        