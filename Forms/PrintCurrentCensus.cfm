<!--- extended the timeout to 5 minutes --->
<cfsetting requesttimeout="300">

<cfquery name="spRunCensusReport_Return" datasource="#REQUEST.Datasource#">
	exec CHS.dbo.spRunCurrentCensus_BH_Rounding_4_Print
    	@FAC_ID = <cfqueryparam value="#TRIM(FAC_ID)#" cfsqltype="cf_sql_varchar">,
        @PStatus = <cfqueryparam value="#TRIM(PStatus)#" cfsqltype="cf_sql_varchar">,
        @Cell_ID = <cfqueryparam value="#TRIM(Cell_ID)#" cfsqltype="cf_sql_varchar">,
        @FromDate = <cfqueryparam value="#TRIM(DateFormat(FromDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">,
        @ToDate = <cfqueryparam value="#TRIM(DateFormat(ToDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">
</cfquery>
  
  
  
  
  <cfset CHS = "CHS">
  
<cfscript>
				//Create New Spreadsheet
				SpreadsheetObj = spreadsheetNew(CHS);	
				//Get Workbook object
				workbook = SpreadsheetObj.getWorkBook();
				//Get sheet by name where you like to add list validation
				sheet = workbook.getSheet(CHS);
				//Create object of required class
				//Populate object with a query. 
				//select distinct b.LastName +'', ''+ b.FirstName as FullName, convert(varchar(10), b.DOB, 120) DOB, a.JailNumber, a.Cell, d2.performedREsult as LVL, NULL Diagnosis, NULL One_2_One, NULL Assaultive, NULL Sharps, NULL Falls, NULL Seizures, NULL ChangeInCondition,	NULL NewOrders, NULL MedRefusal, NULL Etos, NULL Prns, NULL Detox, NULL Diabetes, NULL Pregnant, NULL Comments
				SpreadsheetAddRow(SpreadsheetObj,"NAME,DOB,JAILNUMBER,CELL,LVL,DIAGNOSIS,1 TO 1,ASSAULTIVE,SHARPS,FALLS,SEIZURES,CHANGE IN CONDITION,NEW ORDERS,MED REFUSAL,ETO'S,PRN'S,DETOX,DIABETES,PREGNANT,WOUNDCARE,IVF,IVABTS,ISO,MEALRECORD,COMMENTS");
				SpreadsheetAddRows(SpreadsheetObj,spRunCensusReport_Return);
				//Format rows			
				// Set rotation formating to cell (0,2)
				formatAlignment = StructNew();     
				formatAlignment.alignment="center";			
				SpreadsheetFormatColumns(SpreadsheetObj,formatAlignment,"7-24"); 

				
				// set row height
				//SpreadsheetFormatRows (spreadsheetObj, format, rows)		
				//SpreadsheetFormatRows (spreadsheetObj, format, 1-100)	
				//formatRowHeight = StructNew();
				//formatRowHeight.RowHeight="40";
				//SpreadsheetFormatRows(spreadsheetObj, formatRowHeight, "1-100");
				
				formatAllSheet = StructNew();
				//formatAllSheet.color="blue";
				formatAllSheet.rightborder="thin";				
				formatAllSheet.leftbordercolor="thin";
				formatAllSheet.topborder="thin";
				formatAllSheet.bottomborder="thin";			
				
				//Format by Rows
				//SpreadsheetFormatRows(spreadsheetObj, formatAllSheet, "1-#spRunCensusReport_Return.Recordcount#");
				
				//Format by Columns
				//SpreadsheetFormatColumns(SpreadsheetObj,formatAllSheet,"1-26");				
				
				//Format by Cells - best results
				//SpreadsheetFormatCellRange (spreadsheetObj, format, startRow, startColumn, endRow, endColumn)
				SpreadsheetFormatCellRange(SpreadsheetObj,formatAllSheet,1,1,#spRunCensusReport_Return.Recordcount#+1,25);
				
				
				//SpreadsheetMergeCells(spreadsheetObj, startRow, endRow, startColumn, endColumn)
				//SpreadsheetMergeCells(SpreadsheetObj,2,#spRunCensusReport_Return.Recordcount#,25,26);
				//Set the value of the merged cell. 
    			//SpreadsheetSetCellValue(SpreadsheetObj,"Columns 25-26 of rows 1-3 are merged",1,4); 
				
				//formatAllSheet.rightborder="thick";
				//formatAllSheet.height="40";
				//formatAllSheet.color="blue";
				//SpreadsheetFormatColumns(SpreadsheetObj,formatAllSheet,"1-24");
				 //SpreadsheetFormatCell(SpreadsheetObj,formatAllSheet,"1,3,5");
				
				//SpreadSheetSetRowHeight(SpreadsheetObj, 1, 20);
				
				formatRotation = StructNew();     
				formatRotation.rotation=90;		
				//formatRotation.alignment="center";	
				//formatRotation.size=5;	
				//SpreadsheetFormatCell(SpreadsheetObj,formatRotation,1,9);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,4,5);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,7,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,8,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,9,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,10,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,11,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,12,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,13,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,14,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,15,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,16,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,17,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,18,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,19,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,20,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,21,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,22,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,23,4);
				//SpreadSheetSetColumnWidth(SpreadsheetObj,24,4);
				
				
				
				//SpreadsheetFormatCellRange (spreadsheetObj, format, startRow, startColumn, endRow, endColumn)
				SpreadsheetFormatCellRange(SpreadsheetObj,formatRotation,1,7,1,24);							
				//Set the headers of the file
       			SpreadsheetSetHeader(SpreadsheetObj,"SHIFT:  7X3 (   )  3X11 (   )  11X7 (   )"," CHS Rounding by #SESSION.BHRounding.UserName#","#DateFormat(NOW(),'MM/DD/YYYY')#");
				// write to folder
				//spreadsheetwrite(SpreadsheetObj,"D:\WebApps\BHRounding\Reports\test.xls",true);
				//spreadsheetwrite(SpreadsheetObj,"#expandpath('./QR/test.xls')#",true);
	        </cfscript>
            
            
            <!---	wrte to browser --->
            <cfheader name="Content-Disposition" value="attachment; filename=CHS_REPORT.xls">
			<cfcontent type="application/msexcel" variable="#SpreadSheetReadBinary(SpreadsheetObj)#" reset="yes"></cfcontent>
  
  
 <cfabort>
  
  
<!---============================================= CLOSE PROCESSING =============================================--->
<!---
<cfdump var="#spRunCensusReport_Return#">
<cfabort>

<cfheader name="Content-Disposition" value="attachment; filename=CHS_REPORT.xls">
<cfcontent type="application/msexcel" reset="yes"> 

<table width="100%" border="1">
    <tr>
                <th></th>
                <th align="center">Name</th>
                <th class="th_bg">DOB</th>
                <th align="center">JailNumber</th>
                <th align="center">Cell</th> 
                <th align="center">Level</th>
                <th align="center">Diagnosis</th>
                <th align="center">1 to 1</th>
                <th align="center">ASSULTIVE</th>
                <th align="center">SHARPS</th>
                <th align="center">FALLS</th>
                <th align="center">SEIZURES</th>
                <th align="center">CHANGE IN CONDITION</th>
                <th align="center">NEW ORDERS</th>
                <th align="center">MED REFUSAL</th>
                <th align="center">ETO'S</th>
                <th align="center">PRN'S</th>
                <th align="center">DETOX</th>
                <th align="center">DIABETES</th>
                <th align="center">PREGNANT</th>
                <th align="center">Comments</th>                               
            </tr>   
            <cfoutput query="spRunCensusReport_Return">
            <tr align="left">
            	<td align="center">#CurrentRow#</td>
           		<td align="left">#FullName#</td>
                <td align="center">#DateFormat(DOB,'mm/dd/yyyy')#</td>
            	<td align="center">#JailNumber#</td>
                <td align="center">#Cell#</td> 
                <td>#performedREsult#</td>   
                <td></td>  
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>  
            </tr>
            </cfoutput>
</table>
</cfcontent>

--->












