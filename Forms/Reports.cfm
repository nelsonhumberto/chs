<!---<cfdump var="#FORM#">--->

<cfparam name="FAC_ID" default="">
<cfparam name="Cell_ID" default="">
<cfparam name="PStatus" default="">
<cfparam name="FromDate" default="">
<cfparam name="ToDate" default="">
<cfparam name="FromReportDate" default="">
<cfparam name="ToReportDate" default="">

<!--- extended the timeout to 5 minutes --->
<cfsetting requesttimeout="300">

<cfquery name="spListFacilities_Return" datasource="#REQUEST.Datasource#">
	exec CHS.dbo.spListFacilities       
</cfquery> 

<cfif NOT isDefined('RunReportButton')>
	<cfset FAC_ID = 0>
</cfif>

<cfquery name="spRunCensusReport_Return" datasource="#REQUEST.Datasource#">
	exec CHS.dbo.spRunCensusReport_BH_Rounding
    	@FAC_ID = <cfqueryparam value="#TRIM(FAC_ID)#" cfsqltype="cf_sql_varchar">,
        @Cell_ID = <cfqueryparam value="#TRIM(Cell_ID)#" cfsqltype="cf_sql_varchar">,
        @PStatus = <cfqueryparam value="#TRIM(PStatus)#" cfsqltype="cf_sql_varchar">,
        @FromDate = <cfqueryparam value="#TRIM(DateFormat(FromDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">,
        @ToDate = <cfqueryparam value="#TRIM(DateFormat(ToDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">,
        @FromReportDate = <cfqueryparam value="#TRIM(DateFormat(FromReportDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">,
        @ToReportDate = <cfqueryparam value="#TRIM(DateFormat(ToReportDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">
</cfquery>

<div class="row placeholders">
    <div class="embed-responsive-item-16by9">
    </div>
</div>

<h3 class="sub-header text-primary">Reports</h3>

<div class="table-responsive">
    <table class="table table-striped table-hover"><!---	f9f9f9	--->
    	<tr bgcolor="#f9f9f9">
        	<cfform name="CensusReportsForm" method="post">
        	<td>            
            	<strong>Facility</strong>
            	<cfselect name="FAC_ID" id="idReportFAC_IDInput" query="spListFacilities_Return" queryPosition="below" value="FAC_ID" display="FAC_Name" selected="#FAC_ID#" onchange="CheckReportUnit()">
                	<option value="">All Facilities</option>
                </cfselect>
                <select class="form-control" name="Cell" id="idReportCellInput" style="display:none; width:160px; float:right" multiple="multiple">
                        <option value="">Pick Unit</option>
                        <option value="k10" <cfif ListFind(Cell_ID,'k10')>selected="yes"</cfif>>k10</option>
                        <option value="K21" <cfif ListFind(Cell_ID,'K21')>selected="yes"</cfif>>K21</option>                    
                        <option value="K22" <cfif ListFind(Cell_ID,'K22')>selected="yes"</cfif>>K22</option>                    
                        <option value="K23" <cfif ListFind(Cell_ID,'K23')>selected="yes"</cfif>>K23</option>
                        <option value="K24" <cfif ListFind(Cell_ID,'K24')>selected="yes"</cfif>>K24</option>
                        <option value="K31" <cfif ListFind(Cell_ID,'K31')>selected="yes"</cfif>>K31</option>
                        <option value="K32" <cfif ListFind(Cell_ID,'K32')>selected="yes"</cfif>>K32</option>
                        <option value="K33" <cfif ListFind(Cell_ID,'K33')>selected="yes"</cfif>>K33</option>
                        <option value="K34" <cfif ListFind(Cell_ID,'K34')>selected="yes"</cfif>>K34</option>
                        <option value="K41" <cfif ListFind(Cell_ID,'K41')>selected="yes"</cfif>>K41</option>
                        <option value="K43" <cfif ListFind(Cell_ID,'K43')>selected="yes"</cfif>>K43</option>
                        <option value="K46" <cfif ListFind(Cell_ID,'K46')>selected="yes"</cfif>>K46</option>
                        <option value="K51" <cfif ListFind(Cell_ID,'K51')>selected="yes"</cfif>>K51</option>
                        <option value="K53" <cfif ListFind(Cell_ID,'K53')>selected="yes"</cfif>>K53</option>
                        <option value="k111" <cfif ListFind(Cell_ID,'K111')>selected="yes"</cfif>>k111</option>
                        <option value="k110" <cfif ListFind(Cell_ID,'K110')>selected="yes"</cfif>>k110</option>
                        <option value="Infirmary" <cfif ListFind(Cell_ID,'Infirmary')>selected="yes"</cfif>>Infirmary</option>
                	</select>
            </td>
            <td>
            	<strong>Status</strong><br>
            	<select name="PStatus" id="idReportPStatus">
                	<option value="">Pick One</option>
                    <option value="R" <cfif PStatus eq 'R'>selected="selected"</cfif>>Released</option>
                    <option value="N" <cfif PStatus eq 'N'>selected="selected"</cfif>>Incarcerated</option>
                </select>
            </td>
            <td><div style="position:relative; z-index:1" id="FromDateID">
            	<strong>From</strong><br>
            	<cfoutput><input type="date" class="DatePicker" name="FromDate" id="idReportFromDateInput" size="10" value="#DateFormat(FromDate,'mm/dd/yyyy')#" title="FROM DATE"></cfoutput></div>
            </td>
            <td><div style="position:relative; z-index:1" id="ToDateID">      
            	<strong>To</strong><br>
            	<cfoutput><input type="date" class="DatePicker" name="ToDate" id="idReportToDateInput" value="#DateFormat(ToDate,'mm/dd/yyyy')#" size="10" title="TO DATE"></cfoutput></div>
            </td>
        </tr>
        <tr bgcolor="#f9f9f9">
        	<td>
            <cfoutput>
            	Report From:&nbsp;<input type="date" class="DatePicker" name="FromReportDate" id="idFromReportDate" size="10" value="#DateFormat(FromReportDate,'mm/dd/yyyy')#" title="FROM DATE">&nbsp;&nbsp;To:&nbsp;<input type="date" class="DatePicker" name="ToReportDate" id="idToReportDate" size="10" value="#DateFormat(ToReportDate,'mm/dd/yyyy')#" title="TO DATE">
            </cfoutput>
            </td>
        	<td>
            	<cfinput type="button" name="RunCensusReportButton" value=" Run " class="button blue radius small nice" onClick="RunCensusReport()"/>
            	<cfinput type="button" name="ResetCensusReportButton" value="Reset Values" class="button blue radius small nice" onClick="ClearCensusReportsForm()"/>
            </td>
            <td></td>
            <td>
            <cfoutput>
			<cfif spRunCensusReport_Return.RecordCount GT 0>
            	<a href="Forms/PrintCensusReport.cfm?FAC_ID=#FAC_ID#&amp;Cell_ID=#Cell_ID#&amp;PStatus=#PStatus#&amp;FromDate=#FromDate#&amp;ToDate=#ToDate#&amp;FromReportDate=#FromReportDate#&amp;ToReportDate=#ToReportDate#" target="_blank"><img src="img/export_excel.png" width="32" height="32" alt="Export to Excel" /></a>
            </cfif>
            </cfoutput>
            </td>
    	</tr>
        </cfform>
        <tr>
        	<td colspan="4">		
              	<a name="CallData" id="ReportCallData">Showing <font color="##006699">&nbsp;</font> <cfoutput>#spRunCensusReport_Return.recordCount#</cfoutput><font color="##006699">&nbsp;</font> Records
            </td>
        </tr>
    </table>
	<table id="idReportsCensus" class="table table-striped table-hover">
    	<thead>
            <tr>

            	<th>Name</th>
                <th>DOB</th>
                <th>JailNumber</th>
                <th>Facility</th>
                <th>Cell</th>
                <th>Booked</th>
                <th>Released</th>
                <th>Rounded On</th>
                <th>Rounded By</th>
            </tr>
        </thead>
        <tbody>
        	<cfoutput query="spRunCensusReport_Return">
            <tr>
            	<td><a href="##" data-toggle="modal" data-target="##ReportModal_BH_Sheet" onClick="ShowReportInmateProfile('#JailNumber#');">#FullName#</a></td>
                <!---<td align="center">#Gender#</td>--->
                <td align="center">#DateFormat(DOB,'mm/dd/yyyy')#</td>
               	<td align="center">#JailNumber#</td>
                <!---<td align="center">#FIN#</td>--->
                <td>#FAC_Name#</td>
                <td align="center">#Cell#</td>
                <td align="center">#DateFormat(Book_DT,"mm/dd/yyyy")#</td>
                <td align="center">#DateFormat(Rel_DT,"mm/dd/yyyy")#</td>
                <td align="center">#DateFormat(AddedOn,"mm/dd/yyyy")# #TimeFormat(AddedOn,"H:m")#</td>
                <td>#AddedBy#</td>
                
                
            </tr>
            </cfoutput>
        </tbody>
	</table>
</div>

<!--- Modal_AttendingBoard	--->



<!--- Attending Modal --->
<div id="ReportModal_BH_Sheet" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="display:none">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="ReportModalHeader_BH_Sheet"></h4>
        <!---<div class="well" align="center" style="font-size:16; font-weight:bold; background-color:#5eace8">CASE DETAIL</div>--->
      </div>
      <div class="modal-body" id="ReportModalBody_BH_Sheet">
      	<ul class="nav nav-tabs">
         	<li class="active"><a data-toggle="tab" href="#idCurrentReportDiv" onclick="ShowReportInmateProfile();">Today</a></li>
            <li><a data-toggle="tab" href="#idHistoryReportDiv" onclick="ShowReportInmateBHHistory();">Rounding History</a></li>
         </ul>   
         <div class="tab-content">
         	<div id="idCurrentReportDiv" class="tab-pane fade in active"></div>
            <div id="idHistoryReportDiv" class="tab-pane fade"></div>     
         </div>   
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-danger" name="CancelReportButton_BH_Sheet" id="idCancelReportButton_BH_Sheet" data-dismiss="modal" value=" Cancel " style="float:left" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="hidden" name="CurrentJailNumber" id="idCurrentReportJailNumber" value="" />
      </div>
    </div>
  </div>
</div>

                    
