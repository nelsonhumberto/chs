<!---============================================= OPEN PROCESSING =============================================--->
<cfparam name="ApptHistoryDisplay" default="none">
<cfparam name="ApptHistoryImage" default="plussignHideShowBlue.gif">
<cfparam name="ShiftID" default="0">


<!---
<cfif isDefined("FORM.AddAppointmentButton")>

	<cfdump var="#FORM#">
	<cfabort>

	<!---	Check to see if appointment exists in DB	--->    
    <cfquery name="spCheckForExistingAppointment_Return" datasource="#REQUEST.Datasource#">    
    	exec spCheckForExistingAppointment
			@JailNumber = <cfqueryparam value="#TRIM(FORM.JailNumber)#" cfsqltype="cf_sql_varchar">,
			@ApptTypeID = <cfqueryparam value="#TRIM(FORM.ApptTypeID)#" cfsqltype="cf_sql_integer">
	</cfquery>
    
    <cfif spCheckForExistingAppointment_Return.RecordCount neq 0>
    	<script> 
			alert('This appointment already exists for patient. Please reschedule existing appointment or mark it as Seen or Not Seen!');
		</script>
        
        <cfset ApptHistoryDisplay = "">
    	<cfset ApptHistoryImage = "minussignHideShowBlue.gif">
        
	<cfelse>
        <cfquery name="spAddUpdateAppointment_Return" datasource="#REQUEST.Datasource#">
            exec spAddUpdateAppointment
                @JailNumber = <cfqueryparam value="#TRIM(FORM.JailNumber)#" cfsqltype="cf_sql_varchar">,
                @LogID = <cfqueryparam value="#TRIM(FORM.LogID)#" cfsqltype="cf_sql_integer">,
                @ApptDate = <cfqueryparam value="#TRIM(FORM.ApptDate)#" cfsqltype="cf_sql_varchar">,
                @ApptTypeID = <cfqueryparam value="#TRIM(FORM.SelectedApptTypeID)#" cfsqltype="cf_sql_integer">,
                @DateRequested = <cfqueryparam value="#TRIM(FORM.DateRequested)#" cfsqltype="cf_sql_varchar">,
                @TimeRequested = <cfqueryparam value="#TRIM(FORM.TimeRequested)#" cfsqltype="cf_sql_varchar">,
                @Urgent = <cfqueryparam value="#TRIM(FORM.Urgent)#" cfsqltype="cf_sql_bit">,
                @UserName = <cfqueryparam value="#TRIM(Session.CHS.UserName)#" cfsqltype="cf_sql_varchar">
        </cfquery>

    	<cfset ApptHistoryDisplay = "">
    	<cfset ApptHistoryImage = "minussignHideShowBlue.gif">
	</cfif>
</cfif>
--->

<cfquery name="spGetInmateProfile_Return" datasource="#REQUEST.Datasource#">
	exec CHS.dbo.spGetInmateProfile_BH_Rounding    
    @JailNumber = <cfqueryparam value="#JailNumber#" cfsqltype="cf_sql_varchar">
</cfquery>

<cfquery name="spGetInmate_BH_RoundingData_S1_Return" datasource="#REQUEST.Datasource#">
    exec CHS.dbo.spGetInmate_BH_RoundingData
    	@JailNumber = <cfqueryparam value="#JailNumber#" cfsqltype="cf_sql_varchar">,
    	@ShiftID = <cfqueryparam value="1" cfsqltype="cf_sql_integer">
</cfquery>


<!---============================================= CLOSE PROCESSING =============================================--->


	

<form name="InmateProfileForm">
<cfoutput>

<!---<cfinput type="hidden" name="LogID" value="#spGetInmateActivities_Return.LogID#">--->
<table border="2" width="100%">
	<tr>
    	<td colspan="2">
        <table class="con-table_Details" width="100%" cellpadding="0" cellspacing="0">
        	<tr class="table_bg">
              	<td align="center">
                <h4>Reports &nbsp;<button type="button" class="button btn-default" data-dismiss="modal" style="float:right; margin-right:10px">&times;</button></h4>
              	</td>
          	</tr>	
     	</table>
        </td>
    </tr>
    <tr>
    	<td colspan="2">
        <table class="con-table_Details" width="100%" cellpadding="3" cellspacing="3">

            <tr><td colspan="4" class="TdProfileGrey">&nbsp;</td></tr>
        	<tr class="con-td-detail">
    			<td class="TdProfileBlue" width="20%">&nbsp;Last Name: </td>
        		<td class="TdProfileGrey" width="30%">&nbsp;#spGetInmateProfile_Return.LastName#</td>
                <td class="TdProfileBlue">&nbsp;Jail Number: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.JailNumber#</td>            	
            </tr>
            <tr class="con-td-detail">
    			<td class="TdProfileBlue" width="20%">&nbsp;First Name: </td>
        		<td class="TdProfileGrey" width="30%">&nbsp;#spGetInmateProfile_Return.FirstName#</td>
                <td class="TdProfileBlue">&nbsp;CIN ##: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.CIN#</td>
            </tr>
            <tr class="con-td-detail">
    			<td class="TdProfileBlue">&nbsp;Middle Name: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.MiddleName#</td>
            	<td class="TdProfileBlue">&nbsp;Facility: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.FAC_NAME#</td>
            </tr>
            <tr>
            	<td class="TdProfileBlue">&nbsp;DOB: </td>
        		<td class="TdProfileGrey">&nbsp;#DateFormat(spGetInmateProfile_Return.DOB, 'mm/dd/yyyy')#</td>
                <td class="TdProfileBlue">&nbsp;Cell: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.CELL#</td>
            </tr>
            <tr>
            	<td class="TdProfileBlue">&nbsp;Gender: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.Gender#</td>
                <td class="TdProfileBlue">&nbsp;Booked:</td>
        		<td class="TdProfileGrey">&nbsp;#DateFormat(spGetInmateProfile_Return.BOOK_DT,'mm/dd/yyyy')#</td>
                
            </tr>
            <tr>
            	<td class="TdProfileBlue">&nbsp;Height: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.Height#</td>
               	<td class="TdProfileBlue">&nbsp;Cell Date Change: </td>
        		<td class="TdProfileGrey">&nbsp;#DateFormat(spGetInmateProfile_Return.CELL_DT,'mm/dd/yyyy')#</td>
            </tr>
            <tr>
            	<td class="TdProfileBlue">&nbsp;Weight: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.Weight#</td>
               	<td class="TdProfileBlue">&nbsp;Status: </td>
        		<td class="TdProfileGrey">&nbsp;
					<cfif spGetInmateProfile_Return.PStatus eq "N">
                    	Incarcerated
					<cfelseif spGetInmateProfile_Return.PStatus eq "R">
                    	Released
                    <cfelse>
                    	#spGetInmateProfile_Return.PStatus#
                    </cfif>
                </td>
            </tr>
            <tr>
            	<td class="TdProfileBlue">&nbsp;Race: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.Race#</td>
                <td class="TdProfileBlue">&nbsp;Released:</td>
        		<td class="TdProfileGrey">&nbsp;<cfif spGetInmateProfile_Return.REL_DT neq "">#DateFormat(spGetInmateProfile_Return.REL_DT,'mm/dd/yyyy')#&nbsp;(#spGetInmateProfile_Return.REL_TM#)</cfif></td>
            </tr>
            <tr>
            	<td class="TdProfileBlue">&nbsp;LVL:</td>
                <td class="TdProfileGrey" colspan="3">&nbsp;#spGetInmateProfile_Return.LVL#</td>                
            </tr>
            <tr>
            	<td class="TdProfileBlue">&nbsp;Diagnosis:</td>
                <td class="TdProfileGrey" colspan="3">&nbsp;#spGetInmateProfile_Return.Diagnosis#</td>                
            </tr>
            <tr><td colspan="4">&nbsp;<div id="BH_MessageDiv" style="display:none; color:##0C0"></div></td></tr>
            <tr>
				<td colspan="4">
                <table border="0" width="100%" cellpadding="2" cellspacing="2">

                	<tr>
                    	<td class="TdProfileGrey" id="idSfift1">
                            <div class="form-group" style="margin-left:3px">
                        	<input type="hidden" name="nRecordID1" id="idRecordID1" value="#spGetInmate_BH_RoundingData_S1_Return.RecordID#"  />
                            <input type="checkbox" name="nOne_2_One1" id="idOne_2_One1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.One_2_One eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idOne_2_One1"><span class="normal-font-weight">1 to 1</span></label>
                            <br />
                            <input type="checkbox" name="nAssaultive1" id="idAssaultive1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Assaultive eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idAssaultive1"><span class="normal-font-weight">Assaultive</span></label>
                            <br />
                            <input type="checkbox" name="nSharps1" id="idSharps1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Sharps eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idSharps1"><span class="normal-font-weight">Sharps</span></label>
                            <br />
                             <input type="checkbox" name="nFalls1" id="idFalls1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Falls eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idFalls1"><span class="normal-font-weight">Falls</span></label>
                            <br />
                             <input type="checkbox" name="nSeizures1" id="idSeizures1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Seizures eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idSeizures1"><span class="normal-font-weight">Seizures</span></label>
                            <br />
                             <input type="checkbox" name="nChangeInCondition1" id="idChangeInCondition1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.ChangeInCondition eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idChangeInCondition1"><span class="normal-font-weight">Change in conditions</span></label>
                            <br />
                            <input type="checkbox" name="nNewOrders1" id="idNewOrders1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.NewOrders eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idNewOrders1"><span class="normal-font-weight">New Orders</span></label>
                            <br />
                            <input type="checkbox" name="nMedRefusal1" id="idMedRefusal1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.MedRefusal eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idMedRefusal1"><span class="normal-font-weight">Med Refusal</span></label>
                            <br />
                            <input type="checkbox" name="nEtos1" id="idEtos1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Etos eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idEtos1"><span class="normal-font-weight">ETO's</span></label>
                            <br />
                            <input type="checkbox" name="nPrns1" id="idPrns1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Prns eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idPrns1"><span class="normal-font-weight">PRN'S</span></label>
                            <br />
                            <input type="checkbox" name="nDetox1" id="idDetox1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Detox eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idDetox1"><span class="normal-font-weight">Detox</span></label>
                            <br />
                            <input type="checkbox" name="nDiabetes1" id="idDiabetes1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Diabetes eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idDiabetes1"><span class="normal-font-weight">Diabetes</span></label>
                            <br />
                            <input type="checkbox" name="nPregnant1" id="idPregnant1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Pregnant eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idPregnant1"><span class="normal-font-weight">Pregnant</span></label>
                            <br />
                            <input type="checkbox" name="nWoundCare1" id="idWoundCare1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.WoundCare eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idWoundCare1"><span class="normal-font-weight">WoundCare</span></label>
                            <br />
                            <input type="checkbox" name="nIVF1" id="idIVF1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.IVF eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idIVF1"><span class="normal-font-weight">IVF</span></label>
                            <br />
                            <input type="checkbox" name="nIVABTS1" id="idIVABTS1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.IVABTS eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idIVABTS1"><span class="normal-font-weight">IVABTS</span></label>
                            <br />
                            <input type="checkbox" name="nIso1" id="idIso1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Iso eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idIso1"><span class="jhs-normal-font-weight">Isolation</span></label>
                            <br />           
                            <input type="checkbox" name="nMealRecord1" id="idMealRecord1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.MealRecord eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idMealRecord1"><span class="jhs-normal-font-weight">MealRecord</span></label>
                            <br />                 
                            <textarea name="nComments1" id="idComments1" wrap="virtual" rows="5" cols="20" class="cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.Comments#</textarea>
                            <br />
							Nurse: #spGetInmate_BH_RoundingData_S1_Return.AddedBy#
                        </div>
                        </td>

                        </td>
                    </tr>
                </table>            	
            </td>
        </tr>
        <!---
        <TR><td colspan="4"></td></TR>
        <tr class="TdProfileGrey">        	
            <td><input type="button" class="btn btn-success" name="SaveButton_BH_Sheet" id="idSaveButton_BH_Sheet" onClick="Save_BH_Rounding();" value=" SAVE " style="margin-left:10px" disabled="disabled" /></td>
        	<td>&nbsp;<input type="button" class="btn btn-danger" name="CancelButton_BH_Sheet" id="idCancelButton_BH_Sheet" data-dismiss="modal" value=" Cancel " /></td>
            <td></td>
            <td align="right"><button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right:10px">Close</button></td>
        </tr>
        <TR><td colspan="4" class="TdProfileGrey">&nbsp;</td></TR>
		--->
</table>
</cfoutput>
</form>


