<form name="LookupInmateForm" id="LookupInmateForm">
<table class="table" border="0" cellpadding="0" cellspacing="0">
    <tr><td colspan="2" align="center"><font color="#336699"><b>Lookup Inmate</b></font></td></tr>
    <tr>
        <td colspan="2">
        	&nbsp;    Jail #:&nbsp;(130012345)
        	&nbsp;<input type="text" id="idSearchJailNumber" name="nSearchJailNumber" onKeyDown="CheckEnterSubmit()" />
    	</td>
    </tr>
    <tr><td height="5"></td></tr>
    <tr>
        <td colspan="2">
        	&nbsp;Name:&nbsp;(Last, First)
			&nbsp;<input type="text" id="idSearchFullName" name="nSearchFullName" onKeyDown="CheckEnterSubmit()" /><br />
			
        </td>
    </tr>
    <tr><td height="5"></td></tr>
	<tr>
        <td colspan="2">
        	&nbsp;CIN:&nbsp;(03123456)
        	&nbsp;<input type="text" id="idSearchCIN" name="nSearchCIN" onKeyDown="CheckEnterSubmit()" />
        </td>
    </tr>
    <tr><td height="5"></td></tr>
    <tr>
        <td colspan="2">
        	&nbsp;DOB:&nbsp;(mm/dd/yyyy)&nbsp;
        	&nbsp;<input type="datefield" id="idSearchDOB" name="nSearchDOB" onKeyDown="CheckEnterSubmit()" />
    	</td>
    </tr>
    <tr><td height="5"></td></tr>
    <!---
    <tr>
        <td colspan="2">
        	&nbsp;Cell Number:&nbsp;
        	&nbsp;<cfinput type="text" id="CellNumber" name="Number" />
    	</td>
    </tr>
    <tr><td height="5"></td></tr>
	--->
    <tr>
        <td colspan="2" align="center">
        <input name="ClearSearchForm" type="button" value="Clear" onclick="ClearLookupInmateForm();" style="background-color:##BFD2EC">
        <input name="Search" type="button" value="Search" onclick="LookupInmate();" style="background-color:##BFD2EC">
        </td>
    </tr>
</table>
</form>   