<cfquery name="spLookupInmate_Return" datasource="CHS">
	exec CHS.dbo.spLookupInmateBH
    	@JailNumber = <cfqueryparam value="#JailNumber#" cfsqltype="cf_sql_varchar">, 
        @FullName = <cfqueryparam value="#FullName#" cfsqltype="cf_sql_varchar">, 
        @CIN = <cfqueryparam value="#CIN#" cfsqltype="cf_sql_varchar">,
        @DOB = <cfqueryparam value="#DOB#" cfsqltype="cf_sql_varchar">
</cfquery>

<!---
<cfif spLookupInmate_Return.RecordCount eq 1>   
	
	
	<cfoutput>
    <script type="text/jscript">
		ShowSearchInmateProfile('#spLookupInmate_Return.JailNumber#');
	</script>
	</cfoutput>
	
</cfif>
--->

<div class="row placeholders">
    <div class="embed-responsive-item-16by9">
    </div>
</div>

<h3 class="sub-header text-primary">Lookup Inmate</h3>

<div class="table-responsive">
	<table id="idLookupInmateDT" class="table table-striped table-hover">
    	<thead>
        	<tr>
            	<th>Name</th>
                <th>DOB</th>
                <th>JailNumber</th>
                <th>Facility</th>
                <th>Cell</th>
                <th>Booked</th>
                <th><font style="text-decoration:"Released</th>
            </tr>
        </thead>
        <tbody>
        	<cfoutput query="spLookupInmate_Return">
        	<tr>
            	<td><a href="##" data-toggle="modal" data-target="##SearchModal_BH_Sheet" onClick="ShowSearchInmateProfile('#JailNumber#');">#FullName#</a></td>
                <!---<td align="center">#Gender#</td>--->
                <td align="center">#DateFormat(DOB,'mm/dd/yyyy')#</td>
               	<td align="center">#JailNumber#</td>
                <!---<td align="center">#FIN#</td>--->
                <td>#FAC_Name#</td>
                <td align="center">#Cell#</td>
                <td align="center">#DateFormat(Book_DT,"mm/dd/yyyy")#</td>
                <td align="center">#DateFormat(Rel_DT,"mm/dd/yyyy")#</td>
            </tr>
            </cfoutput> 
        </tbody>
	</table>
</div>

<!--- Modal_AttendingBoard	--->

<!--- Attending Modal --->
<div id="SearchModal_BH_Sheet" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="display:none">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="SearchModalHeader_BH_Sheet"></h4>
        <!---<div class="well" align="center" style="font-size:16; font-weight:bold; background-color:#5eace8">CASE DETAIL</div>--->
      </div>
      <div class="modal-body" id="SearchModalBody_BH_Sheet">
      	<ul class="nav nav-tabs">
         	<li class="active"><a data-toggle="tab" href="#idCurrentSearchDiv" onclick="ShowSearchInmateProfile();">Today</a></li>
            <li><a data-toggle="tab" href="#idHistorySearchDiv" onclick="ShowSearchInmateBHHistory();">Rounding History</a></li>
         </ul>   
         <div class="tab-content">
         	<div id="idCurrentSearchDiv" class="tab-pane fade in active"></div>
            <div id="idHistorySearchDiv" class="tab-pane fade"></div>     
         </div>   
      </div>
      <!------>
	  <div class="modal-footer">      	
        <input type="button" class="btn btn-success" name="SaveSearchButton_BH_Sheet" id="idSaveSearchButton_BH_Sheet" onClick="Save_BH_Rounding();" value=" SAVE " style="float:left" disabled="disabled" />
        <input type="button" class="btn btn-danger" name="CanceSearchlButton_BH_Sheet" id="idCanceSearchlButton_BH_Sheet" data-dismiss="modal" value=" Cancel " style="float:left" />
        <input type="hidden" name="CurrentJailNumber" id="idCurrentSearchJailNumber" value="" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>		
      </div>
	  
    </div>
  </div>
</div>
                    
        