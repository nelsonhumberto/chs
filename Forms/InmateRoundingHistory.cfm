
<cfquery name="spGetInmateRoundingHistory_Return" datasource="#REQUEST.Datasource#">
	exec CHS_Dev.dbo.spGetInmateRoundingHistory
    	@JailNumber = <cfqueryparam value="#TRIM(JailNumber)#" cfsqltype="cf_sql_varchar">
</cfquery>

<div class="row placeholders">
    <div class="embed-responsive-item-16by9">
    </div>
</div>

<!---<h3 class="sub-header text-primary">Rounding History</h3>--->

<div class="table-responsive">
	<table border="2" width="100%">
    	<tr>
    		<td colspan="2">
        	<table class="con-table_Details" width="100%" cellpadding="0" cellspacing="0">
        		<tr class="table_bg">
              		<td align="center">
                		<h4><cfoutput>#spGetInmateRoundingHistory_Return.FullName#</cfoutput> &nbsp;<button type="button" class="button btn-default" data-dismiss="modal" style="float:right; margin-right:10px">&times;</button></h4>	
              		</td>
          		</tr>	
     		</table>
        	</td>
    	</tr>
    </table>
	<table id="idRoundingHistoryDT" class="table table-striped table-hover">
    	<thead>
        	<tr>
            	<th>Date</th>
                <th>Shift</th>
                <th>Cell</th>
                <th>LVL</th>
                <th>Dx</th>
                <th>Hx</th>
                <th>Procedures</th>
                <th>Diagnostic</th>
                <th>Fall<br />Precaution</th>
                <th>Suicide<br />Precaution</th>
                <th>Seizures<br />Precaution</th>
                <th>WoundCare</th>
                <th>WoundCare<br />Comments</th>
                <th>IVF</th>
                <th>IVF<br />Comments</th>
                <th>IVABx</th>
                <th>IVABx<br />Comments</th>
                <th>Glucose Check</th>
                <th>Glucose Check<br />Comments</th>
                <th>Med Refusal</th>
                <th>Med Refusal<br />Comments</th>
                <th>Labs</th>
                <th>Labs<br />Comments</th>
                <th>Pregnant</th>
                <th>Pregnant<br />Comments</th>
                <th>PRN's</th>
                <th>PRN's<br />Comments</th>
                <th>ETO's</th>
                <th>New Orders</th>
                <th>New Orders<br />Comments</th>
                <th>Change in conditions</th>
                <th>Change in conditions<br />Comments</th>
                <th>Isolation</th>
                <th>Isolation<br />Comments</th>
                <th>MealRecord</th>
                <th>MealRecord<br />Comments</th>
                <th>CIWA/COVOS</th>
                <th>CIWA/COVOS<br />Comments</th>
                <th>Out Of Cell</th>
                <th>Out Of Cell<br />Comments</th>
                <th>Detox</th>
                <th>Detox<br />Comments</th>
                <th>Comments</th>
                <th>Nurse</th>
            </tr>
        </thead>
        <tbody>
        	<cfoutput query="spGetInmateRoundingHistory_Return">
        	<tr>
            	<td>#DateFormat(AddedOn,'mm/dd/yyyy')#  #TimeFormat(AddedOn,'HH:MM')#</td>
                <td align="center">#ShiftID#</td>
               	<td align="center">#Cell#</td>
                <td>#LevelText#</td>
                <td align="center">#Admitting#</td>
                <td align="center">#MedicalHistory#</td>
                <td align="center">#Procedures#</td>
                <td align="center">#Diagnostic#</td>
                <td align="center">#Falls#</td>
                <td align="center">#Suicide#</td>
                <!---<td align="center">#SuicideText#</td>--->
                <td align="center">#Seizures#</td>
                <td align="center">#WoundCare#</td>
                <td align="center">#WoundCareText#</td>
                <td align="center">#IVF#</td>
                <td align="center">#IVFText#</td>
                <td align="center">#IVABTS#</td>
                <td align="center">#IVABTSText#</td>
                <td align="center">#Diabetes#</td>
                <td align="center">#DiabetesText#</td>
                <td align="center">#Medrefusal#</td>
                <td align="center">#MedrefusalText#</td>
                <td align="center">#Labs#</td>
                <td align="center">#LabsText#</td>
                <td align="center">#Pregnant#</td>
                <td align="center">#PregnantText#</td>
                <td align="center">#Prns#</td>
                <td align="center">#PrnsText#</td>
                <td align="center">#ETOS#</td>
                <td align="center">#NewOrders#</td>
                <td align="center">#NewOrdersText#</td>
                <td align="center">#ChangeInCondition#</td>
                <td align="center">#ChangeInConditionText#</td>
                <td align="center">#Iso#</td>
                <td align="center">#IsoText#</td>
                <td align="center">#MealRecord#</td>
                <td align="center">#MealRecordText#</td>
                <td align="center">#Ciwa#</td>
                <td align="center">#CiwaText#</td>
                <td align="center">#OutOfCell#</td>
                <td align="center">#OutOfCellText#</td>
                <td align="center">#Detox#</td>
                <td align="center">#DetoxText#</td>
                <td align="center">#Comments#</td>
                <td align="center">#AddedBy#</td>


            </tr>
            </cfoutput> 
        </tbody>
	</table>
</div>

<!--- Modal_AttendingBoard	--->

<!--- Attending Modal 
<div id="Modal_BH_Sheet" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="display:none">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="ModalHeader_BH_Sheet"></h4>
        <!---<div class="well" align="center" style="font-size:16; font-weight:bold; background-color:#5eace8">CASE DETAIL</div>--->
      </div>
      <div class="modal-body" id="ModalBody_BH_Sheet">
      	 <ul class="nav nav-tabs">
         	<li class="active"><a data-toggle="tab" href="#idCurrentDiv">Today</a></li>
            <li><a data-toggle="tab" href="#idHistoryDiv" onClick="LoadContent('AttendingBoard');">Rounding History</a></li>
         </ul>   
         <div class="tab-content">
         	<div id="idCurrentDiv" class="tab-pane fade in active"></div>
            <div id="idHistoryDiv" class="tab-pane fade"></div>     
         </div>   
      </div>
      <!------>
	  <div class="modal-footer">      
        <input type="button" class="btn btn-success" name="SaveButton_BH_Sheet" id="idSaveButton_BH_Sheet" onClick="Save_BH_Rounding();" value=" SAVE " style="float:left" disabled="disabled" />
        <input type="button" class="btn btn-danger" name="CancelButton_BH_Sheet" id="idCancelButton_BH_Sheet" data-dismiss="modal" value=" Cancel " style="float:left" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>		
      </div>
	  
    </div>
  </div>
</div>
--->

<!---
<div class="table-responsive">
    <table class="table table-striped table-hover">
      <thead>
        <tr>
            <th>Facility</th>                
            <th>Capacity</th>
            <th>Census</th>
            <th>%</th>
        </tr>
      </thead>
      <tbody>
       <cfoutput query="GetCensusReport">
        <tr class="#Class#">
            <td align="center">#Facility#</td>
            <td align="center">#FunctionalCapacity#</td>
            <td>#TotalCensus#</td>
            <td>#CurrentCensusCapacity#%</td>
        </tr>
        </cfoutput>                            
      </tbody>
    </table>
</div>
--->
                    
        