<!--- extended the timeout to 5 minutes --->
<cfsetting requesttimeout="300">

<cfquery name="spRunCensusReport_Return" datasource="#REQUEST.Datasource#">
	exec CHS.dbo.spRunCensusReport_BH_Rounding_4_Print
    	@FAC_ID = <cfqueryparam value="#TRIM(FAC_ID)#" cfsqltype="cf_sql_varchar">,
        @PStatus = <cfqueryparam value="#TRIM(PStatus)#" cfsqltype="cf_sql_varchar">,
        @Cell_ID = <cfqueryparam value="#TRIM(Cell_ID)#" cfsqltype="cf_sql_varchar">,
        @FromDate = <cfqueryparam value="#TRIM(DateFormat(FromDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">,
        @ToDate = <cfqueryparam value="#TRIM(DateFormat(ToDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">,
        @FromReportDate = <cfqueryparam value="#TRIM(DateFormat(FromReportDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">,
        @ToReportDate = <cfqueryparam value="#TRIM(DateFormat(ToReportDate,'mm/dd/yyyy'))#" cfsqltype="cf_sql_varchar">
</cfquery>
  
  
  
  
  <cfset CHS = "CHS">
  
<cfscript>
				//Create New Spreadsheet
				SpreadsheetObj = spreadsheetNew(CHS);	
				//Get Workbook object
				workbook = SpreadsheetObj.getWorkBook();
				//Get sheet by name where you like to add list validation
				sheet = workbook.getSheet(CHS);
				//Create object of required class
				//Populate object with a query. 
				//SpreadsheetAddRow(SpreadsheetObj,"SponsorDisplayName,SponsorEmail,SponsorBadge,ContractorBadge,ContractorDisplayName,ContractorFullName,ContractorEmail,AgencyName,ExpirationDate,ExpiringInDays,Active,Additional Comments (Write In)");
				SpreadsheetAddRow(SpreadsheetObj,"Shift,Name,DOB,JailNumber,Cell,LVL,Diagnosis,1 to 1,Assaultive,Sharps,Falls,Seizures,Change in condition,New orders,Med refusal,Eto's,Prn's,Detox,Diabetes,Pregnant,WoundCare,IVF,IVABTS,Iso,MealRecord,Comments,Nurse,AddedOn");
				SpreadsheetAddRows(SpreadsheetObj,spRunCensusReport_Return);
				
				formatAlignment = StructNew();     
				formatAlignment.alignment="center";			
				SpreadsheetFormatColumns(SpreadsheetObj,formatAlignment,"7-24"); 
				
				formatAllSheet = StructNew();
				//formatAllSheet.color="blue";
				formatAllSheet.rightborder="thin";				
				formatAllSheet.leftbordercolor="thin";
				formatAllSheet.topborder="thin";
				formatAllSheet.bottomborder="thin";			
				
				//Format by Rows
				//SpreadsheetFormatRows(spreadsheetObj, formatAllSheet, "1-#spRunCensusReport_Return.Recordcount#");
				
				//Format by Columns
				//SpreadsheetFormatColumns(SpreadsheetObj,formatAllSheet,"1-26");				
				
				//Format by Cells - best results
				//SpreadsheetFormatCellRange (spreadsheetObj, format, startRow, startColumn, endRow, endColumn)
				SpreadsheetFormatCellRange(SpreadsheetObj,formatAllSheet,1,1,#spRunCensusReport_Return.Recordcount#+1,28);
				
				//Format rows			
				// Set rotation formating to cell (0,2)
				formatRotation = StructNew();     
				formatRotation.rotation=90;		
				//formatRotation.alignment="center";	
				//formatRotation.size=5;	
				//SpreadsheetFormatCell(SpreadsheetObj,formatRotation,1,9);
				SpreadSheetSetColumnWidth(SpreadsheetObj,1,2);
				SpreadSheetSetColumnWidth(SpreadsheetObj,4,5);
				SpreadSheetSetColumnWidth(SpreadsheetObj,7,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,8,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,9,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,10,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,11,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,12,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,13,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,14,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,15,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,16,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,17,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,18,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,19,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,20,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,21,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,22,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,23,4);
				SpreadSheetSetColumnWidth(SpreadsheetObj,24,4);
				//SpreadsheetFormatCellRange (spreadsheetObj, format, startRow, startColumn, endRow, endColumn)
				SpreadsheetFormatCellRange(SpreadsheetObj,formatRotation,1,8,1,25);				
				//Set the headers of the file
       			SpreadsheetSetHeader(SpreadsheetObj,"SHIFT: 7X3(   ) 3X11(   ) 11X7(   )"," CHS Rounding Report. Printed by #SESSION.BHRounding.UserName#","#DateFormat(NOW(),'MM/DD/YYYY')#");
				// write to folder
				//spreadsheetwrite(SpreadsheetObj,"D:\WebApps\BHRounding\Reports\test.xls",true);
				//spreadsheetwrite(SpreadsheetObj,"#expandpath('./QR/test.xls')#",true);
	        </cfscript>
            
            
            <!---	wrte to browser --->
            <cfheader name="Content-Disposition" value="attachment; filename=CHS_REPORT.xls">
			<cfcontent type="application/msexcel" variable="#SpreadSheetReadBinary(SpreadsheetObj)#" reset="yes"></cfcontent>
  
  
 <cfabort>
  
  
<!---============================================= CLOSE PROCESSING =============================================--->
<!---
<cfdump var="#spRunCensusReport_Return#">
<cfabort>

<cfheader name="Content-Disposition" value="attachment; filename=CHS_REPORT.xls">
<cfcontent type="application/msexcel" reset="yes"> 

<table width="100%" border="1">
    <tr>
                <th></th>
                <th align="center">Name</th>
                <th class="th_bg">DOB</th>
                <th align="center">JailNumber</th>
                <th align="center">Cell</th> 
                <th align="center">Level</th>
                <th align="center">Diagnosis</th>
                <th align="center">1 to 1</th>
                <th align="center">ASSULTIVE</th>
                <th align="center">SHARPS</th>
                <th align="center">FALLS</th>
                <th align="center">SEIZURES</th>
                <th align="center">CHANGE IN CONDITION</th>
                <th align="center">NEW ORDERS</th>
                <th align="center">MED REFUSAL</th>
                <th align="center">ETO'S</th>
                <th align="center">PRN'S</th>
                <th align="center">DETOX</th>
                <th align="center">DIABETES</th>
                <th align="center">PREGNANT</th>
                <th align="center">Comments</th>                               
            </tr>   
            <cfoutput query="spRunCensusReport_Return">
            <tr align="left">
            	<td align="center">#CurrentRow#</td>
           		<td align="left">#FullName#</td>
                <td align="center">#DateFormat(DOB,'mm/dd/yyyy')#</td>
            	<td align="center">#JailNumber#</td>
                <td align="center">#Cell#</td> 
                <td>#performedREsult#</td>   
                <td></td>  
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>  
            </tr>
            </cfoutput>
</table>
</cfcontent>

--->












