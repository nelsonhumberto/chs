<!---============================================= OPEN PROCESSING =============================================--->
<cfparam name="ApptHistoryDisplay" default="none">
<cfparam name="ApptHistoryImage" default="plussignHideShowBlue.gif">
<cfparam name="ShiftID" default="0">
<link rel="stylesheet" href="css/main.css">

<!---
<cfif isDefined("FORM.AddAppointmentButton")>

	<cfdump var="#FORM#">
	<cfabort>

	<!---	Check to see if appointment exists in DB	--->    
    <cfquery name="spCheckForExistingAppointment_Return" datasource="#REQUEST.Datasource#">    
    	exec spCheckForExistingAppointment
			@JailNumber = <cfqueryparam value="#TRIM(FORM.JailNumber)#" cfsqltype="cf_sql_varchar">,
			@ApptTypeID = <cfqueryparam value="#TRIM(FORM.ApptTypeID)#" cfsqltype="cf_sql_integer">
	</cfquery>
    
    <cfif spCheckForExistingAppointment_Return.RecordCount neq 0>
    	<script> 
			alert('This appointment already exists for patient. Please reschedule existing appointment or mark it as Seen or Not Seen!');
		</script>
        
        <cfset ApptHistoryDisplay = "">
    	<cfset ApptHistoryImage = "minussignHideShowBlue.gif">
        
	<cfelse>
        <cfquery name="spAddUpdateAppointment_Return" datasource="#REQUEST.Datasource#">
            exec spAddUpdateAppointment
                @JailNumber = <cfqueryparam value="#TRIM(FORM.JailNumber)#" cfsqltype="cf_sql_varchar">,
                @LogID = <cfqueryparam value="#TRIM(FORM.LogID)#" cfsqltype="cf_sql_integer">,
                @ApptDate = <cfqueryparam value="#TRIM(FORM.ApptDate)#" cfsqltype="cf_sql_varchar">,
                @ApptTypeID = <cfqueryparam value="#TRIM(FORM.SelectedApptTypeID)#" cfsqltype="cf_sql_integer">,
                @DateRequested = <cfqueryparam value="#TRIM(FORM.DateRequested)#" cfsqltype="cf_sql_varchar">,
                @TimeRequested = <cfqueryparam value="#TRIM(FORM.TimeRequested)#" cfsqltype="cf_sql_varchar">,
                @Urgent = <cfqueryparam value="#TRIM(FORM.Urgent)#" cfsqltype="cf_sql_bit">,
                @UserName = <cfqueryparam value="#TRIM(Session.CHS.UserName)#" cfsqltype="cf_sql_varchar">
        </cfquery>

    	<cfset ApptHistoryDisplay = "">
    	<cfset ApptHistoryImage = "minussignHideShowBlue.gif">
	</cfif>
</cfif>
--->

<cfquery name="spGetInmateProfile_Return" datasource="#REQUEST.Datasource#">
	exec CHS_Dev.dbo.spGetInmateProfile_BH_Rounding
    @JailNumber = <cfqueryparam value="#JailNumber#" cfsqltype="cf_sql_varchar">     
</cfquery>

<cfquery name="spGetInmate_BH_RoundingData_S1_Return" datasource="#REQUEST.Datasource#">
    exec CHS_Dev.dbo.spGetInmate_BH_RoundingData
    	@JailNumber = <cfqueryparam value="#JailNumber#" cfsqltype="cf_sql_varchar">,
    	@ShiftID = <cfqueryparam value="1" cfsqltype="cf_sql_integer">      
</cfquery>
<cfquery name="spGetInmate_BH_RoundingData_S2_Return" datasource="#REQUEST.Datasource#">
    exec CHS_Dev.dbo.spGetInmate_BH_RoundingData
    	@JailNumber = <cfqueryparam value="#JailNumber#" cfsqltype="cf_sql_varchar">,
    	@ShiftID = <cfqueryparam value="2" cfsqltype="cf_sql_integer">      
</cfquery>
<cfquery name="spGetInmate_BH_RoundingData_S3_Return" datasource="#REQUEST.Datasource#">
    exec CHS_Dev.dbo.spGetInmate_BH_RoundingData
    	@JailNumber = <cfqueryparam value="#JailNumber#" cfsqltype="cf_sql_varchar">,
    	@ShiftID = <cfqueryparam value="3" cfsqltype="cf_sql_integer">      
</cfquery>


<!---============================================= CLOSE PROCESSING =============================================--->


	

<form name="InmateProfileForm">
<cfoutput>
<input type="hidden" name="nJailNumber" id="idJailNumber" value="#spGetInmateProfile_Return.JailNumber#">
<input type="hidden" name="nCELL" id="idCELL" value="#spGetInmateProfile_Return.CELL#">
<input type="hidden" name="nLVL" id="idLVL" value="#spGetInmateProfile_Return.LVL#">
<input type="hidden" name="nDiagnosis" id="idDiagnosis" value="#spGetInmateProfile_Return.Diagnosis#">
<input type="hidden" name="nShiftID" id="idShiftID" />
<!---<cfinput type="hidden" name="LogID" value="#spGetInmateActivities_Return.LogID#">--->
<table border="2" width="100%">
	<tr>
    	<td colspan="2">
        <table class="con-table_Details" width="100%" cellpadding="0" cellspacing="0">
        	<tr class="table_bg">
              	<td align="center">
                <h4>#spGetInmateProfile_Return.FullName# &nbsp;<button type="button" class="button btn-default" data-dismiss="modal" style="float:right; margin-right:10px">&times;</button></h4>
              	</td>
          	</tr>	
     	</table>
        </td>
    </tr>
    <tr>
    	<td colspan="2">
        <table class="con-table_Details" width="100%" cellpadding="3" cellspacing="3">
        	<tr class="con-td-detail">
                <td class="TdProfileBlue">&nbsp;Select Shift: </td>
        		<td class="TdProfileBlue" colspan="3">
                            <label><input type="radio" name="Shift" class="RadioButton" onclick="PickShift('1')" <cfif spGetInmate_BH_RoundingData_S1_Return.RecordID neq 0 and Session.BHRounding.UserName neq spGetInmate_BH_RoundingData_S1_Return.AddedBy>disabled="disabled"</cfif>> 7X3</label>&nbsp;
                            <label><input type="radio" name="Shift" class="RadioButton" onclick="PickShift('2')" <cfif spGetInmate_BH_RoundingData_S2_Return.RecordID neq 0 and Session.BHRounding.UserName neq spGetInmate_BH_RoundingData_S1_Return.AddedBy>disabled="disabled"</cfif>> 3X11</label>&nbsp;
                            <label><input type="radio" name="Shift" class="RadioButton" onclick="PickShift('3')" <cfif spGetInmate_BH_RoundingData_S3_Return.RecordID neq 0 and Session.BHRounding.UserName neq spGetInmate_BH_RoundingData_S1_Return.AddedBy>disabled="disabled"</cfif>> 11X7</label>&nbsp;

                </td>
            </tr>
            <tr><td colspan="4" class="TdProfileGrey">&nbsp;</td></tr>
        	<tr class="con-td-detail">
    			<td class="TdProfileBlue" width="25%">&nbsp;Last Name: </td>
        		<td class="TdProfileGrey" width="30%">&nbsp;#spGetInmateProfile_Return.LastName#</td>
                <td class="TdProfileBlue">&nbsp;Jail Number: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.JailNumber#</td>            	
            </tr>
            <tr class="con-td-detail">
    			<td class="TdProfileBlue" width="25%">&nbsp;First Name: </td>
        		<td class="TdProfileGrey" width="30%">&nbsp;#spGetInmateProfile_Return.FirstName#</td>
                <td class="TdProfileBlue">&nbsp;CIN ##: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.CIN#</td>
            </tr>
            <tr class="con-td-detail">
    			<td class="TdProfileBlue">&nbsp;Middle Name: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.MiddleName#</td>
                <td class="TdProfileBlue">&nbsp;DOB: </td>
        		<td class="TdProfileGrey">&nbsp;#DateFormat(spGetInmateProfile_Return.DOB, 'mm/dd/yyyy')#</td>
            	<!---<td class="TdProfileBlue">&nbsp;Facility: </td>--->
        		<!---<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.FAC_NAME#</td>--->
            </tr>
            <tr>
                <td class="TdProfileBlue">&nbsp;Height: </td>
            <td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.Height#</td>
                <td class="TdProfileBlue">&nbsp;Cell: </td>
        		<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.CELL#</td>
            </tr>
            <tr>
                 <td class="TdProfileBlue">&nbsp;Weight: </td>
                    <td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.Weight#</td>
                <td class="TdProfileBlue">&nbsp;Booked:</td>
        		<td class="TdProfileGrey">&nbsp;#DateFormat(spGetInmateProfile_Return.BOOK_DT,'mm/dd/yyyy')#</td>
                
            </tr>

            <tr>
            	<!---<td class="TdProfileBlue">&nbsp;Race: </td>--->
        		<!---<td class="TdProfileGrey">&nbsp;#spGetInmateProfile_Return.Race#</td>--->
                <!---<td class="TdProfileBlue">&nbsp;Released:</td>--->
        		<!---<td class="TdProfileGrey">&nbsp;<cfif spGetInmateProfile_Return.REL_DT neq "">#DateFormat(spGetInmateProfile_Return.REL_DT,'mm/dd/yyyy')#&nbsp;(#spGetInmateProfile_Return.REL_TM#)</cfif></td>--->
            </tr>
            <tr>
            	<td class="TdProfileBlue">&nbsp;LVL:</td>
                <td class="TdProfileGrey" colspan="3">&nbsp;#spGetInmateProfile_Return.LVL#</td>                
            </tr>

            <tr><td colspan="4">&nbsp;<div id="BH_MessageDiv" style="display:none; color:##0C0"></div></td></tr>
            <tr>
				<td colspan="4">
                <table border="0" width="100%" cellpadding="2" cellspacing="2">
                	<tr>
                    	<td class="TdProfileGrey" id="idSfift1" style="">
<table style="width:100%">

    <tr id="" style="display:">
        <td class="TdProfileBlue">&nbsp;Level:</td>
        <td class="TdProfileGrey" colspan="4"><textarea name="nLevelText1" id="idLevelText1" wrap="virtual" rows="1" cols="20"class="cInputSfift1" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S1_Return.LevelText#</textarea>
    </tr>
    <tr id="" style="display:">
        <td class="TdProfileBlue">&nbsp;Admitting (DX):</td>
        <td class="TdProfileGrey" colspan="4"><textarea name="nAdmitting1" id="idAdmitting1" wrap="virtual" rows="1" cols="20" class="cInputSfift1" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S1_Return.Admitting#</textarea>
    </tr>
    <tr id="" style="display:" >
        <td class="TdProfileBlue">&nbsp;History (HX):</td>
        <td class="TdProfileGrey" colspan="4"><textarea name="nMedicalHistory1" id="idMedicalHistory1" wrap="virtual" rows="1" cols="20"class="cInputSfift1" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S1_Return.MedicalHistory#</textarea>
    </tr>
    <tr id="" style="display:" >
        <td class="TdProfileBlue">&nbsp;Procedure:</td>
        <td class="TdProfileGrey" colspan="4"><textarea name="nProcedures1" id="idProcedures1" wrap="virtual" rows="1" cols="20" class="cInputSfift1" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S1_Return.Procedures#</textarea>
    </tr>
    <tr id="" style="display:" >
        <td class="TdProfileBlue">&nbsp;Diagnostic:</td>
        <td class="TdProfileGrey" colspan="4"><textarea name="nDiagnostic1" id="idDiagnostic1" wrap="virtual" rows="1" cols="20" class="cInputSfift1" disabled="disabled" style="width:600px; max-width: 600px "#spGetInmate_BH_RoundingData_S1_Return.Diagnostic#></textarea>
    </tr>

                                <div class="form-group" style="margin-left:3px ;">

                        	<input type="hidden" name="nRecordID1" id="idRecordID1" value="#spGetInmate_BH_RoundingData_S1_Return.RecordID#"  />
<tr>
                              <td>
                            <input type="checkbox" name="nFalls1" id="idFalls1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Falls eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idFalls1"><span class="jhs-normal-font-weight">Fall Precaution</span></label>
                            </td>
                               <td>
                            <input type="checkbox" name="nIVF1" id="idIVF1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.IVF eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idIVF1"><span  class="jhs-normal-font-weight">

                            <a data-toggle="popover" data-trigger="hover"  title="IntraVenous Fluids" data-content="IntraVenous Fluids">IVF</a>



                            </span></label>
<textarea name="nIVFText1" id="idIVFText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.IVFText#</textarea>

</td>
                            <td>
                            <input type="checkbox" name="nPregnant1" id="idPregnant1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Pregnant eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idPregnant1"><span class="jhs-normal-font-weight">Pregnant</span></label>
<textarea name="nPregnantText1" id="idPregnantText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.PregnantText#</textarea>

                            </td>
                           <td>
                            <input type="checkbox" name="nIso1" id="idIso1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Iso eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idIso1"><span class="jhs-normal-font-weight">Isolation</span></label>
<textarea name="nIsoText1" id="idIsoText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.IsoText#</textarea>

</td>
</tr>

<tr>
                            <td>
                            <input type="checkbox" name="nSuicide1" id="idSuicide1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Suicide eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idSuicide1"><span class="jhs-normal-font-weight">Suicide Precaution</span></label>
                            </td>
                              <td>

                            <input type="checkbox" name="nIVABTS1" id="idIVABTS1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.IVABTS eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idIVABTS1"><span class="jhs-normal-font-weight">

                                        <a data-toggle="popover" data-trigger="hover"  title="IntraVenous Antibiotics " data-content="IntraVenous Antibiotics">IVABx</a>



                            </span></label>
<textarea name="nIVABTSText1" id="idIVABTSText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.IVABTSText#</textarea>

</td>
                           <td>
                            <input type="checkbox" name="nPrns1" id="idPrns1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Prns eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idPrns1"><span class="jhs-normal-font-weight">PRN'S</span></label>
<textarea name="nPrnsText1" id="idPrnsText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.PrnsText#</textarea>

</td>
                            <td>
                            <input type="checkbox" name="nMealRecord1" id="idMealRecord1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.MealRecord eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idMealRecord1"><span class="jhs-normal-font-weight">MealRecord</span></label>
<textarea name="nMealRecordText1" id="idMealRecordText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.MealRecordText#</textarea>

</td>
</tr>
<tr>
                         <td>
                            <input type="checkbox" name="nSeizures1" id="idSeizures1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Seizures eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idSeizures1"><span class="jhs-normal-font-weight">Seizures Precaution</span></label>
                            </td>
                              <td>
                            <input type="checkbox" name="nDiabetes1" id="idDiabetes1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Diabetes eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idDiabetes1"><span class="jhs-normal-font-weight">Glucose Check</span></label>
<textarea name="nDiabetesText1" id="idDiabetesText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.DiabetesText#</textarea>

</td>
                            <td>

                            <input type="checkbox" name="nEtos1" id="idEtos1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Etos eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idEtos1"><span class="jhs-normal-font-weight">ETO's</span></label>
                            </td>
                            <td>

                            <input type="checkbox" name="nCiwa1" id="idCiwa1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Ciwa eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idCiwa1"><span class="jhs-normal-font-weight">CIWA/COVOS</span></label>
<textarea name="nCiwaText1" id="idCiwaText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.CiwaText#</textarea>

</td>
</tr>
<tr>
                              <td>

                            <input type="checkbox" name="nSharps1" id="idSharps1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Sharps eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idSharps1"><span class="jhs-normal-font-weight">Sharps</span></label>
                            </td>
                              <td>
                            <input type="checkbox" name="nMedRefusal1" id="idMedRefusal1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.MedRefusal eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idMedRefusal1"><span class="jhs-normal-font-weight">Med Refusal</span></label>
<textarea name="nMedRefusalText1" id="idMedRefusalText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.MedRefusalText#</textarea>

</td>
                             <td>
                            <input type="checkbox" name="nNewOrders1" id="idNewOrders1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.NewOrders eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idNewOrders1"><span class="jhs-normal-font-weight">New Orders</span></label>
<textarea name="nNewOrdersText1" id="idNewOrdersText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.NewOrdersText#</textarea>

                            </td>
                            <td>

                            <input type="checkbox" name="nOutOfCell1" id="idOutOfCell1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.OutofCell eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idOutOfCell1"><span class="jhs-normal-font-weight">Out Of Cell</span></label>
<textarea name="nOutOfCellText1" id="idOutOfCellText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.OutofCellText#</textarea>

</td>
</tr>
<tr>
                               <td>
                            <input type="checkbox" name="nWoundCare1" id="idWoundCare1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.WoundCare eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idWoundCare1"><span class="jhs-normal-font-weight">WoundCare

                            </span></label>
<textarea name="idWoundCareText1" id="idWoundCareText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.WoundCareText#</textarea>




</td>
                            <td>

                            <input type="checkbox" name="nLabs1" id="idLabs1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Labs eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idLabs1"><span class="jhs-normal-font-weight">Labs

</span></label>
<textarea name="nLabsText1" id="idLabsText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.LabsText#</textarea>

                            </td>
                            <td>

                             <input type="checkbox" name="nChangeInCondition1" id="idChangeInCondition1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.ChangeInCondition eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idChangeInCondition1"><span class="jhs-normal-font-weight">Change in conditions
<textarea name="nChangeInConditionText1" id="idChangeInConditionText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.ChangeInConditionText#</textarea>

</span></label>

                            </td>
                            <td>
                            <input type="checkbox" name="nDetox1" id="idDetox1" value="1" class="cInputSfift1" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S1_Return.Detox eq 1>checked="yes"</cfif>>
                            <label style="display: inline" for="idDetox1"><span class="jhs-normal-font-weight">Detox
<textarea name="nDetoxText1" id="idDetoxText1" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.DetoxText#</textarea>

</span></label>
                            </td>




</tr>
<tr coldspan="4">
    <td colspan="4">


    <textarea name="nComments1" id="idComments1" wrap="virtual" rows="3" class="cInputSfift1" disabled="disabled" placeholder="Comments..." style="width: 700px;max-width:700px ">#spGetInmate_BH_RoundingData_S1_Return.Comments#</textarea>

    <!---
    <input id="msg" type="text" class="form-control" name="msg" placeholder="Comments">
    --->
<!---
<textarea name="nComments1" id="idComments1" wrap="virtual" rows="5" cols="20" class="cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.Comments#</textarea>

--->

                            </td>
</tr>

</div>
</table>

							Nurse: #spGetInmate_BH_RoundingData_S1_Return.AddedBy#

                        </td>

                        <td class="TdProfileGrey" id="idSfift2" style="display:none">
                        <table style="width:100%">

     <tr id="" style="display:">
        <td class="TdProfileBlue">&nbsp;Level:</td>
        <td class="TdProfileGrey" colspan="4"><textarea name="nLevelText2" id="idLevelText2" wrap="virtual" rows="1" cols="20"class="cInputSfift2" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S2_Return.LevelText#</textarea>
    </tr>
    <tr id="" style="display:">
        <td class="TdProfileBlue">&nbsp;Admitting (DX):</td>
        <td class="TdProfileGrey" colspan="4"><textarea name="nAdmitting2" id="idAdmitting2" wrap="virtual" rows="1" cols="20"class="cInputSfift2" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S2_Return.Admitting#</textarea>
    </tr>

    <tr id="" style="display:">
        <td class="TdProfileBlue">&nbsp;History (HX):</td>
        <td class="TdProfileGrey" colspan="4"><textarea name="nMedicalHistory2" id="idMedicalHistory2" wrap="virtual" rows="1" cols="20"class="cInputSfift2" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S2_Return.MedicalHistory#</textarea>
    </tr>
    <tr id="" style="display:">
        <td class="TdProfileBlue">&nbsp;Procedure:</td>
        <td class="TdProfileGrey" colspan="4"><textarea name="nProcedure2" id="idProcedures2" wrap="virtual" rows="1" cols="20"class="cInputSfift2" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S2_Return.Procedures#</textarea>
    </tr>
    <tr id="" style="display:">
        <td class="TdProfileBlue">&nbsp;Diagnostic:</td>
        <td class="TdProfileGrey" colspan="4"><textarea name="nDiagnostic2" id="idDiagnostic2" wrap="virtual" rows="1" cols="20"class="cInputSfift2" disabled="disabled"  style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S2_Return.Diagnostic#</textarea>
    </tr>






                        <div class="form-group" style="margin-left:3px ;">

                                <input type="hidden" name="nRecordID2" id="idRecordID2" value="#spGetInmate_BH_RoundingData_S2_Return.RecordID#"  />
<tr>
<td>
        <input type="checkbox" name="nFalls2" id="idFalls2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Falls eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idFalls2"><span class="jhs-normal-font-weight">Fall Precaution</span></label>
</td>
<td>
        <input type="checkbox" name="nIVF2" id="idIVF2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.IVF eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idIVF2"><span  class="jhs-normal-font-weight">

                            <a data-toggle="popover" data-trigger="hover"  title="IntraVenous Fluids" data-content="IntraVenous Fluids">IVF</a>



                            </span></label>
<textarea name="nIVFText2" id="idIVFText2" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift2" disabled="disabled">#spGetInmate_BH_RoundingData_S2_Return.IVFText#</textarea>

</td>
<td>
        <input type="checkbox" name="nPregnant2" id="idPregnant2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Pregnant eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idPregnant2"><span class="jhs-normal-font-weight">Pregnant</span></label>
<textarea name="nPregnantText2" id="idPregnantText2" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift2" disabled="disabled">#spGetInmate_BH_RoundingData_S2_Return.PregnantText#</textarea>


</td>
<td>
        <input type="checkbox" name="nIso2" id="idIso2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Iso eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idIso2"><span class="jhs-normal-font-weight">Isolation</span></label>
<textarea name="nIsoText2" id="idIsoText2" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift2" disabled="disabled">#spGetInmate_BH_RoundingData_S2_Return.IsoText#</textarea>

</td>
</tr>

<tr>
<td>
        <input type="checkbox" name="nSuicide2" id="idSuicide2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Suicide eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idSuicide2"><span class="jhs-normal-font-weight">Suicide Precaution</span></label>
</td>
<td>

        <input type="checkbox" name="nIVABTS2" id="idIVABTS2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.IVABTS eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idIVABTS2"><span class="jhs-normal-font-weight">

                                        <a data-toggle="popover" data-trigger="hover"  title="IntraVenous Antibiotics " data-content="IntraVenous Antibiotics">IVABx</a>



                            </span></label>
<textarea name="nIVABTSText2" id="idIVABTSText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.IVABTSText#</textarea>

</td>
<td>
        <input type="checkbox" name="nPrns2" id="idPrns2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Prns eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idPrns2"><span class="jhs-normal-font-weight">PRN'S</span></label>
<textarea name="nPrnsText2" id="idPrnsText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.PrnsText#</textarea>

</td>
<td>
        <input type="checkbox" name="nMealRecord2" id="idMealRecord2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.MealRecord eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idMealRecord2"><span class="jhs-normal-font-weight">MealRecord</span></label>
<textarea name="nMealRecordText2" id="idMealRecordText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.MealRecordText#</textarea>

</td>
</tr>
<tr>
<td>
        <input type="checkbox" name="nSeizures2" id="idSeizures2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Seizures eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idSeizures2"><span class="jhs-normal-font-weight">Seizures Precaution</span></label>
</td>
<td>
        <input type="checkbox" name="nDiabetes2" id="idDiabetes2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Diabetes eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idDiabetes2"><span class="jhs-normal-font-weight">Glucose Check</span></label>
<textarea name="nDiabetesText2" id="idDiabetesText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.DiabetesText#</textarea>

</td>
<td>

        <input type="checkbox" name="nEtos2" id="idEtos2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Etos eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idEtos2"><span class="jhs-normal-font-weight">ETO's</span></label>

</td>
<td>

        <input type="checkbox" name="nCiwa2" id="idCiwa2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Ciwa eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idCiwa2"><span class="jhs-normal-font-weight">CIWA/COVOS</span></label>
<textarea name="nCiwaText2" id="idCiwaText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.CiwaText#</textarea>

</td>
</tr>
<tr>
<td>

        <input type="checkbox" name="nSharps2" id="idSharps2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Sharps eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idSharps2"><span class="jhs-normal-font-weight">Sharps</span></label>
</td>
<td>
        <input type="checkbox" name="nMedRefusal2" id="idMedRefusal2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.MedRefusal eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idMedRefusal2"><span class="jhs-normal-font-weight">Med Refusal</span></label>
<textarea name="nMedRefusalText2" id="idMedRefusalText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.MedRefusalText#</textarea>

</td>
<td>
        <input type="checkbox" name="nNewOrders2" id="idNewOrders2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.NewOrders eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idNewOrders2"><span class="jhs-normal-font-weight">New Orders</span></label>
<textarea name="nNewOrdersText2" id="idNewOrdersText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.NewOrdersText#</textarea>

</td>
<td>

        <input type="checkbox" name="nOutOfCell2" id="idOutOfCell2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.OutOfCell eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idOutOfCell2"><span class="jhs-normal-font-weight">Out Of Cell</span></label>
<textarea name="nOutOfCellText2" id="idOutOfCellText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.OutOfCellText#</textarea>

</td>
</tr>
<tr>
<td>
        <input type="checkbox" name="nWoundCare2" id="idWoundCare2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.WoundCare eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idWoundCare2"><span class="jhs-normal-font-weight">WoundCare

                            </span></label>
<textarea name="idWoundCareText2" id="idWoundCareText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.WoundCareText#</textarea>




</td>
<td>

        <input type="checkbox" name="nLabs2" id="idLabs2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Labs eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idLabs2"><span class="jhs-normal-font-weight">Labs

</span></label>
<textarea name="nLabsText2" id="idLabsText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.LabsText#</textarea>

</td>
<td>

        <input type="checkbox" name="nChangeInCondition2" id="idChangeInCondition2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.ChangeInCondition eq 1>checked="yes"</cfif>>
<label style="display: inline" for="idChangeInCondition2"><span class="jhs-normal-font-weight">Change in conditions
<textarea name="nChangeInConditionText2" id="idChangeInConditionText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.ChangeInConditionText#</textarea>

</span></label>

</td>
<td>
        <input type="checkbox" name="nDetox2" id="idDetox2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Detox eq 1>checked="yes"</cfif>>
<label style="display: inline" for="idDetox2"><span class="jhs-normal-font-weight">Detox
<textarea name="nDetoxText2" id="idDetoxText2" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S2_Return.DetoxText#</textarea>

</span></label>
</td>




</tr>
<tr coldspan="4">
<td colspan="4">


<textarea name="nComments2" id="idComments2" wrap="virtual" rows="3" class="cInputSfift2" disabled="disabled" placeholder="Comments..." style="width: 700px;max-width:700px ">#spGetInmate_BH_RoundingData_S2_Return.Comments#</textarea>

<!---
<input id="msg" type="text" class="form-control" name="msg" placeholder="Comments">
--->
<!---
<textarea name="nComments1" id="idComments1" wrap="virtual" rows="5" cols="20" class="cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.Comments#</textarea>

--->

    </td>
    </tr>

    </div>
    </table>

        Nurse: #spGetInmate_BH_RoundingData_S2_Return.AddedBy#

    </td>

                        <td class="TdProfileGrey" id="idSfift3" style="display:none">

    <table style="width:100%">

        <tr id="idSfiftTable3" style="display:">
            <td class="TdProfileBlue">&nbsp;Level:</td>
            <td class="TdProfileGrey" colspan="4"><textarea name="nLevelText3" id="idLevelText3" wrap="virtual" rows="1" cols="20"class="cInputSfift3" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S3_Return.LevelText#</textarea>
        </tr>
        <tr id="idSfiftTable3" style="display:" >
            <td class="TdProfileBlue">&nbsp;Admitting (DX):</td>
            <td class="TdProfileGrey" colspan="4"><textarea name="nAdmitting3" id="idAdmitting3" wrap="virtual" rows="1" cols="20"class="cInputSfift3" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S3_Return.Admitting#</textarea>
        </tr>

        <tr id="idSfiftTable3" style="display:">
            <td class="TdProfileBlue">&nbsp;History (HX):</td>
            <td class="TdProfileGrey" colspan="4"><textarea name="nMedicalHistory3" id="idMedicalHistory3" wrap="virtual" rows="1" cols="20"class="cInputSfift3" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S3_Return.MedicalHistory#</textarea>
        </tr>
        <tr id="idSfiftTable3" style="display:">
            <td class="TdProfileBlue">&nbsp;Procedure:</td>
            <td class="TdProfileGrey" colspan="4"><textarea name="nProcedures3" id="idProcedures3" wrap="virtual" rows="1" cols="20"class="cInputSfift3" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S3_Return.Procedures#</textarea>
        </tr>
        <tr id="idSfiftTable3" style="display:">
            <td class="TdProfileBlue">&nbsp;Diagnostic:</td>
            <td class="TdProfileGrey" colspan="4"><textarea name="nDiagnostic3" id="idDiagnostic3" wrap="virtual" rows="1" cols="20" class="cInputSfift3" disabled="disabled" style="width:600px; max-width: 600px ">#spGetInmate_BH_RoundingData_S3_Return.Diagnostic#</textarea>
        </tr>



    <div class="form-group" style="margin-left:3px ;">

            <input type="hidden" name="nRecordID3" id="idRecordID3" value="#spGetInmate_BH_RoundingData_S3_Return.RecordID#"  />
<tr>
<td>
        <input type="checkbox" name="nFalls3" id="idFalls3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Falls eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idFalls3"><span class="jhs-normal-font-weight">Fall Precaution</span></label>
</td>
<td>
        <input type="checkbox" name="nIVF3" id="idIVF3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.IVF eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idIVF3"><span  class="jhs-normal-font-weight">

                            <a data-toggle="popover" data-trigger="hover"  title="IntraVenous Fluids" data-content="IntraVenous Fluids">IVF</a>



                            </span></label>
<textarea name="nIVFText3" id="idIVFText3" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift3" disabled="disabled">#spGetInmate_BH_RoundingData_S3_Return.IVFText#</textarea>

</td>
<td>
        <input type="checkbox" name="nPregnant3" id="idPregnant3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Pregnant eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idPregnant3"><span class="jhs-normal-font-weight">Pregnant</span></label>
<textarea name="nPregnantText3" id="idPregnantText3" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift3" disabled="disabled">#spGetInmate_BH_RoundingData_S3_Return.PregnantText#</textarea>


</td>
<td>
        <input type="checkbox" name="nIso3" id="idIso3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Iso eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idIso3"><span class="jhs-normal-font-weight">Isolation</span></label>
<textarea name="nIsoText3" id="idIsoText3" wrap="virtual" rows="1" cols="20" class="expandable cInputSfift3" disabled="disabled">#spGetInmate_BH_RoundingData_S3_Return.IsoText#</textarea>

</td>
</tr>

<tr>
<td>
        <input type="checkbox" name="nSuicide3" id="idSuicide3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Suicide eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idSuicide3"><span class="jhs-normal-font-weight">Suicide Precaution</span></label>
</td>
<td>

        <input type="checkbox" name="nIVABTS3" id="idIVABTS3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.IVABTS eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idIVABTS3"><span class="jhs-normal-font-weight">

                                        <a data-toggle="popover" data-trigger="hover"  title="IntraVenous Antibiotics " data-content="IntraVenous Antibiotics">IVABx</a>



                            </span></label>
<textarea name="nIVABTSText3" id="idIVABTSText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.IVABTSText#</textarea>

</td>
<td>
        <input type="checkbox" name="nPrns3" id="idPrns3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Prns eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idPrns3"><span class="jhs-normal-font-weight">PRN'S</span></label>
<textarea name="nPrnsText3" id="idPrnsText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.PrnsText#</textarea>

</td>
<td>
        <input type="checkbox" name="nMealRecord3" id="idMealRecord3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.MealRecord eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idMealRecord3"><span class="jhs-normal-font-weight">MealRecord</span></label>
<textarea name="nMealRecordText3" id="idMealRecordText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.MealRecordText#</textarea>

</td>
</tr>
<tr>
<td>
        <input type="checkbox" name="nSeizures3" id="idSeizures3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Seizures eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idSeizures3"><span class="jhs-normal-font-weight">Seizures Precaution</span></label>
</td>
<td>
        <input type="checkbox" name="nDiabetes3" id="idDiabetes3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Diabetes eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idDiabetes3"><span class="jhs-normal-font-weight">Glucose Check</span></label>
<textarea name="nDiabetesText3" id="idDiabetesText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.DiabetesText#</textarea>

</td>
<td>

        <input type="checkbox" name="nEtos3" id="idEtos3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Etos eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idEtos3"><span class="jhs-normal-font-weight">ETO's</span></label>

</td>
<td>

        <input type="checkbox" name="nCiwa3" id="idCiwa3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Ciwa eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idCiwa3"><span class="jhs-normal-font-weight">CIWA/COVOS</span></label>
<textarea name="nCiwaText3" id="idCiwaText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.CiwaText#</textarea>

</td>
</tr>
<tr>
<td>

        <input type="checkbox" name="nSharps3" id="idSharps3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Sharps eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idSharps3"><span class="jhs-normal-font-weight">Sharps</span></label>
</td>
<td>
        <input type="checkbox" name="nMedRefusal3" id="idMedRefusal3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.MedRefusal eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idMedRefusal3"><span class="jhs-normal-font-weight">Med Refusal</span></label>
<textarea name="nMedRefusalText3" id="idMedRefusalText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.MedRefusalText#</textarea>

</td>
<td>
        <input type="checkbox" name="nNewOrders3" id="idNewOrders3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.NewOrders eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idNewOrders3"><span class="jhs-normal-font-weight">New Orders</span></label>
<textarea name="nNewOrdersText3" id="idNewOrdersText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.NewOrdersText#</textarea>

</td>
<td>
        <input type="checkbox" name="nOutOfCell3" id="idOutOfCell3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.OutOfCell   eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idOutOfCell3"><span class="jhs-normal-font-weight">Out Of Cell</span></label>
<textarea name="nOutOfCellText3" id="idOutOfCellText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.OutOfCellText#</textarea>

</td>
</tr>
<tr>
<td>
        <input type="checkbox" name="nWoundCare3" id="idWoundCare3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.WoundCare eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idWoundCare3"><span class="jhs-normal-font-weight">WoundCare

                            </span></label>
<textarea name="idWoundCareText3" id="idWoundCareText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.WoundCareText#</textarea>




</td>
<td>

        <input type="checkbox" name="nLabs3" id="idLabs3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Labs eq 1>checked="yes"</cfif>>
    <label style="display: inline" for="idLabs3"><span class="jhs-normal-font-weight">Labs

</span></label>
<textarea name="nLabsText3" id="idLabsText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.LabsText#</textarea>

</td>
<td>

        <input type="checkbox" name="nChangeInCondition3" id="idChangeInCondition3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.ChangeInCondition eq 1>checked="yes"</cfif>>
<label style="display: inline" for="idChangeInCondition3"><span class="jhs-normal-font-weight">Change in conditions
<textarea name="nChangeInConditionText3" id="idChangeInConditionText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.ChangeInConditionText#</textarea>

</span></label>

</td>
<td>
        <input type="checkbox" name="nDetox3" id="idDetox3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Detox eq 1>checked="yes"</cfif>>
<label style="display: inline" for="idDetox3"><span class="jhs-normal-font-weight">Detox
<textarea name="nDetoxText3" id="idDetoxText3" wrap="virtual" rows="1" cols="20" class="expandable">#spGetInmate_BH_RoundingData_S3_Return.DetoxText#</textarea>

</span></label>
</td>




</tr>
<tr coldspan="4">
<td colspan="4">


<textarea name="nComments3" id="idComments3" wrap="virtual" rows="3" class="cInputSfift3" disabled="disabled" placeholder="Comments..." style="width: 700px;max-width:700px ">#spGetInmate_BH_RoundingData_S3_Return.Comments#</textarea>

<!---
<input id="msg" type="text" class="form-control" name="msg" placeholder="Comments">
--->
<!---
<textarea name="nComments1" id="idComments1" wrap="virtual" rows="5" cols="20" class="cInputSfift1" disabled="disabled">#spGetInmate_BH_RoundingData_S1_Return.Comments#</textarea>

--->

    </td>
    </tr>

    </div>
    </table>

        Nurse: #spGetInmate_BH_RoundingData_S3_Return.AddedBy#
        </div>

    </td>









                        <!---<td class="TdProfileGrey" id="idSfift2" style="display:none">--->
                        <!---<div class="form-group" style="margin-left:3px">--->
                        	<!---<input type="hidden" name="nRecordID2" id="idRecordID2" value="#spGetInmate_BH_RoundingData_S2_Return.RecordID#"  />--->
                            <!---<input type="checkbox" name="nOne_2_One2" id="idOne_2_One2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.One_2_One eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idOne_2_One2"><span class="jhs-normal-font-weight">1 to 1</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nAssaultive2" id="idAssaultive2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Assaultive eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idAssaultive2"><span class="jhs-normal-font-weight">Assaultive</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nSharps2" id="idSharps2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Sharps eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idSharps2"><span class="jhs-normal-font-weight">Sharps</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nFalls2" id="idFalls2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Falls eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idFalls2"><span class="jhs-normal-font-weight">Falls</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nSeizures2" id="idSeizures2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Seizures eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idSeizures2"><span class="jhs-normal-font-weight">Seizures</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nChangeInCondition2" id="idChangeInCondition2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.ChangeInCondition eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idChangeInCondition2"><span class="jhs-normal-font-weight">Change in conditions</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nNewOrders2" id="idNewOrders2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.NewOrders eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idNewOrders2"><span class="jhs-normal-font-weight">New Orders</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nMedRefusal2" id="idMedRefusal2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.MedRefusal eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idMedRefusal2"><span class="jhs-normal-font-weight">Med Refusal</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nEtos2" id="idEtos2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Etos eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idEtos2"><span class="jhs-normal-font-weight">ETO's</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nPrns2" id="idPrns2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Prns eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idPrns2"><span class="jhs-normal-font-weight">PRN'S</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nDetox2" id="idDetox2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Detox eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idDetox2"><span class="jhs-normal-font-weight">Detox</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nDiabetes2" id="idDiabetes2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Diabetes eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idDiabetes2"><span class="jhs-normal-font-weight">Diabetes</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nPregnant2" id="idPregnant2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Pregnant eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idPregnant2"><span class="jhs-normal-font-weight">Pregnant</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nWoundCare2" id="idWoundCare2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.WoundCare eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idWoundCare2"><span class="jhs-normal-font-weight">WoundCare</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nIVF2" id="idIVF2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.IVF eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idIVF2"><span class="jhs-normal-font-weight">IVF</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nIVABTS2" id="idIVABTS2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.IVABTS eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idIVABTS2"><span class="jhs-normal-font-weight">IVABTS</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nIso2" id="idIso2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.Iso eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idIso2"><span class="jhs-normal-font-weight">Isolation</span></label>--->
                            <!---<br />           --->
                            <!---<input type="checkbox" name="nMealRecord2" id="idMealRecord2" value="1" class="cInputSfift2" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S2_Return.MealRecord eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idMealRecord2"><span class="jhs-normal-font-weight">MealRecord</span></label>--->
                            <!---<br />                 --->
                            <!---<textarea name="nComments2" id="idComments2" wrap="virtual" rows="5" cols="20" class="cInputSfift2" disabled="disabled">#spGetInmate_BH_RoundingData_S2_Return.Comments#</textarea>--->
                            <!---<br />--->
							<!---Nurse: #spGetInmate_BH_RoundingData_S2_Return.AddedBy#--->
                        <!---</div>  --->
                        <!---</td>--->
                        <!---<td class="TdProfileGrey" id="idSfift3" style="display:none">--->
                        <!---<div class="form-group" style="margin-left:3px">--->
                        	<!---<input type="hidden" name="nRecordID3" id="idRecordID3" value="#spGetInmate_BH_RoundingData_S3_Return.RecordID#"  />  --->
                            <!---<input type="checkbox" name="nOne_2_One3" id="idOne_2_One3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.One_2_One eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idOne_2_One3"><span class="jhs-normal-font-weight">1 to 1</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nAssaultive3" id="idAssaultive3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Assaultive eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idAssaultive3"><span class="jhs-normal-font-weight">Assaultive</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nSharps3" id="idSharps3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Sharps eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idSharps3"><span class="jhs-normal-font-weight">Sharps</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nFalls3" id="idFalls3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Falls eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idFalls3"><span class="jhs-normal-font-weight">Falls</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nSeizures3" id="idSeizures3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Seizures eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idSeizures3"><span class="jhs-normal-font-weight">Seizures</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nChangeInCondition3" id="idChangeInCondition3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.ChangeInCondition eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idChangeInCondition3"><span class="jhs-normal-font-weight">Change in conditions</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nNewOrders3" id="idNewOrders3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.NewOrders eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idNewOrders3"><span class="jhs-normal-font-weight">New Orders</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nMedRefusal3" id="idMedRefusal3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.MedRefusal eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idMedRefusal3"><span class="jhs-normal-font-weight">Med Refusal</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nEtos3" id="idEtos3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Etos eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idEtos3"><span class="jhs-normal-font-weight">ETO's</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nPrns3" id="idPrns3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Prns eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idPrns3"><span class="jhs-normal-font-weight">PRN'S</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nDetox3" id="idDetox3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Detox eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idDetox3"><span class="jhs-normal-font-weight">Detox</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nDiabetes3" id="idDiabetes3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Diabetes eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idDiabetes3"><span class="jhs-normal-font-weight">Diabetes</span></label>--->
                            <!---<br />--->
                             <!---<input type="checkbox" name="nPregnant3" id="idPregnant3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Pregnant eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idPregnant3"><span class="jhs-normal-font-weight">Pregnant</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nWoundCare3" id="idWoundCare3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.WoundCare eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idWoundCare3"><span class="jhs-normal-font-weight">WoundCare</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nIVF3" id="idIVF3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.IVF eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idIVF3"><span class="jhs-normal-font-weight">IVF</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nIVABTS3" id="idIVABTS3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.IVABTS eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idIVABTS3"><span class="jhs-normal-font-weight">IVABTS</span></label>--->
                            <!---<br />--->
                            <!---<input type="checkbox" name="nIso3" id="idIso3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.Iso eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idIso3"><span class="jhs-normal-font-weight">Isolation</span></label>--->
                            <!---<br />           --->
                            <!---<input type="checkbox" name="nMealRecord3" id="idMealRecord3" value="1" class="cInputSfift3" disabled="disabled" <cfif spGetInmate_BH_RoundingData_S3_Return.MealRecord eq 1>checked="yes"</cfif>>--->
                            <!---<label style="display: inline" for="idMealRecord3"><span class="jhs-normal-font-weight">MealRecord</span></label>--->
                            <!---<br />                 --->
                            <!---<textarea name="nComments3" id="idComments3" wrap="virtual" rows="5" cols="20" class="cInputSfift3" disabled="disabled">#spGetInmate_BH_RoundingData_S3_Return.Comments#</textarea>--->
                            <!---<br />--->
							<!---Nurse: #spGetInmate_BH_RoundingData_S3_Return.AddedBy#--->
                        <!---</div>  --->
                        <!---</td>--->






                    </tr>
                </table>            	
            </td>
        </tr>
        <!---
        <TR><td colspan="4"></td></TR>
        <tr class="TdProfileGrey">        	
            <td><input type="button" class="btn btn-success" name="SaveButton_BH_Sheet" id="idSaveButton_BH_Sheet" onClick="Save_BH_Rounding();" value=" SAVE " style="margin-left:10px" disabled="disabled" /></td>
        	<td>&nbsp;<input type="button" class="btn btn-danger" name="CancelButton_BH_Sheet" id="idCancelButton_BH_Sheet" data-dismiss="modal" value=" Cancel " /></td>
            <td></td>
            <td align="right"><button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right:10px">Close</button></td>
        </tr>
        <TR><td colspan="4" class="TdProfileGrey">&nbsp;</td></TR>
		--->
</table>
</cfoutput>
</form>


